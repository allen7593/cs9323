from swagger_server.configs import mail_config
from swagger_server.mailService.decorater import set_mail_config
from swagger_server.mailService.decorater import set_mail_config
from flask.logging import default_handler
import logging

logging.basicConfig(filename="comp9323.log", format='%(asctime)s-%(thread)s-%(levelname)s-%(message)s',
                    level=logging.DEBUG)
root = logging.getLogger()
root.addHandler(default_handler)

root.info("Config Email Service")
set_mail_config(mail_config)