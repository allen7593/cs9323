#!/usr/bin/env python3
from logging.config import dictConfig

import connexion

from swagger_server import encoder
from swagger_server.configs import mail_config, db_config
from swagger_server.dataMockup.MockupService import MockUpService
from swagger_server.databaseService import DatabaseService
from swagger_server.loggingService.Logger import set_logger
from flask_cors import CORS

from swagger_server.notificationService.NotificationService import NotificationService


# email config
from swagger_server.qandaService.QAndAService import init_bot


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'COMP9323'}, pythonic_params=True)
    app_logger = app.app.logger
    set_logger(app_logger)
    app_logger.info("Config Database Service")
    CORS(app.app)
    # databse config
    DatabaseService(db_config)
    mockup_service = MockUpService(True)
    mockup_service()
    NotificationService.start(1)
    init_bot()
    app.run(port=8080)


if __name__ == '__main__':
    main()
