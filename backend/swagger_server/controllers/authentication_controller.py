import time

import connexion
import jwt
import six
from werkzeug.exceptions import abort

from swagger_server.databaseService import User
from swagger_server.databaseService.tables.Session import Session
from swagger_server.jwtService import encode_token
from swagger_server.loggingService.Logger import Logger
from swagger_server.models.login import Login  # noqa: E501
from swagger_server import util


def app_generate_token(body):  # noqa: E501
    """Return JWT token

     # noqa: E501

    :param body: User unique identifier
    :type body: dict | bytes

    :rtype: str
    """
    logger = Logger.get_logger()
    logger.debug("Request Authenticating")
    if connexion.request.is_json:
        body: Login = Login.from_dict(connexion.request.get_json())  # noqa: E501
    users: [User] = User.objects(email_address=body.user_name, password=body.password)
    if users.count() > 0:
        logger.debug("Finish Authentication")
        user_session: [Session] = Session.objects(user=users[0])
        jwt_token = encode_token(users[0])
        if user_session.count() > 0:
            current_user_session: Session = user_session[0]
            current_user_session.session_id = jwt_token.decode("utf-8")
            current_user_session.save()
        else:
            session = Session(user=users[0], session_id=jwt_token)
            session.save()
        return jwt_token
    else:
        abort(401)

