from typing import List

import jwt
from werkzeug.exceptions import abort

from swagger_server.jwtService import decode_token
from swagger_server.loggingService.Logger import Logger

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_jwt(token):
    try:
        return decode_token(token)
    except:
        abort(401)
