import connexion
import six
from mongoengine import DoesNotExist

from swagger_server.databaseService import User, Role
from swagger_server.error import error_message
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.notification import Notification as ModelNotification  # noqa: E501
from swagger_server.models.notifications import Notifications  # noqa: E501
from swagger_server import util, utils
from swagger_server.databaseService.tables.Activity import Notification as DBNotification
from swagger_server.notificationService.NotificationService import *
from swagger_server.utils.NotificationUtil import convert_notification_to_dict, convert_notification_to_dict_for_create


def create_notification(body):  # noqa: E501
    """Create New Notification

     # noqa: E501

    :param body: Notification Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = ModelNotification.from_dict(connexion.request.get_json())  # noqa: E501
    new_notification_dict: dict = convert_notification_to_dict_for_create(body)
    new_notification: DBNotification = DBNotification(**new_notification_dict)
    if new_notification.instance_notification:
        try:
            existing_notification: DBNotification = DBNotification.objects.get(name=new_notification_dict["name"])
            existing_notification.update(**new_notification_dict)
            existing_notification.save()
            note_to_pub = existing_notification
        except DoesNotExist:
            note_to_pub = new_notification
            new_notification.save()
        NotificationService.publish(note_to_pub)
        note_to_pub.update(published=True)
        note_to_pub.save()
    else:
        try:
            existing_notification: DBNotification = DBNotification.objects.get(name=new_notification_dict["name"])
            existing_notification.update(**new_notification_dict)
            existing_notification.save()
        except DoesNotExist:
            new_notification.save()
    return 'OK'


def get_notifications():  # noqa: E501
    """Get User Notifications

    Get User Notifications # noqa: E501


    :rtype: Notifications
    """
    user: User = utils.get_current_user()
    if not user:
        return error_message("No User found", 401, "Unauthorized"), 401
    roles = user.roles
    notifications = DBNotification.objects(published=True)
    result_list = []
    for role in roles:
        for notification in notifications:
            if role.role_name in [r.role_name for r in notification.target_roles]:
                if notification not in result_list:
                    result_list.append(notification)
    result_list = sorted(result_list, key=lambda item: item.release_date + timedelta(days=-item.pre_release_days))
    return [convert_notification_to_dict(result) for result in result_list]
