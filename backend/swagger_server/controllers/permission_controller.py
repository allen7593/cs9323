import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.error import error_message
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.permission import Permission as modelPermission  # noqa: E501
from swagger_server import util
from swagger_server.databaseService.tables.Permission import Permission as DBPermission
from swagger_server.utils import convert_permission_to_dict, convert_permission_to_dict_for_update


def create_permission(body):  # noqa: E501
    """Create new Permission

     # noqa: E501

    :param body: Permission Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = modelPermission.from_dict(connexion.request.get_json())  # noqa: E501
    new_permission_dict = convert_permission_to_dict(body)
    current_permission: [DBPermission] = DBPermission.objects(_id=body._id)
    if current_permission.count() < 1:
        new_permission = DBPermission(**new_permission_dict)
        new_permission.save()
        return 'ok'

    else:
        return error_message("Duplicate Term Found", 400), 400


def get_permissions():  # noqa: E501
    """Get all permissions

     # noqa: E501


    :rtype: List[Permission]
    """
    all_permissions = DBPermission.objects()
    return [convert_permission_to_dict(permission) for permission in all_permissions]


def update_permission(body):  # noqa: E501
    """Update exsiting Permission

     # noqa: E501

    :param body: Role Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = modelPermission.from_dict(connexion.request.get_json())  # noqa: E501
    update_permission_dict = convert_permission_to_dict_for_update(body)
    current_permission: [DBPermission] = DBPermission.objects(_id=body._id)
    if current_permission.count() < 1:
        return error_message("Permission not found", 404), 404
    else:
        current_permission[0].update(**update_permission_dict)
        current_permission[0].save()
        return 'ok'
