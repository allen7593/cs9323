import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.databaseService import Project
from swagger_server.error import error_message
from swagger_server.loggingService.Logger import Logger
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.project import Project as ModelProject  # noqa: E501
from swagger_server.models.projects import Projects  # noqa: E501
from swagger_server import util
from swagger_server.utils.ProjectUtil import *


def create_project(body):  # noqa: E501
    """Create new Project

     # noqa: E501

    :param body: Project Information
    :type body: dict | bytes

    :rtype: None
    """
    logger = Logger.get_logger()
    if connexion.request.is_json:
        body: ModelProject = ModelProject.from_dict(connexion.request.get_json())  # noqa: E501
    new_project_dict = convert_project_to_dict_for_create(body)
    if Project.objects(name=new_project_dict["name"]).count() > 0:
        return error_message(f"Project: {new_project_dict['name']} has already exist", 400), 400
    new_project = Project(**new_project_dict)
    try:
        logger.info("Saving New Project")
        new_project.save()
        logger.info("New Project Saved")
    except Exception as e:
        logger.error(e)
        abort(400)
    return 'OK'


def get_project_by_ids(projectids: list):  # noqa: E501
    """Get all available projects

     # noqa: E501

    :param projectids: 
    :type projectids: List[str]

    :rtype: None
    """
    return_list = Project.objects(id__in=projectids)
    return [convert_project_to_dict(p) for p in return_list]


def get_projects():  # noqa: E501
    """Get all available projects

     # noqa: E501


    :rtype: Projects
    """
    projects = Project.objects()
    project_list = [convert_project_to_dict(p) for p in projects]
    return sorted(project_list, key=lambda project: project["name"].lower())


def update_project(body):  # noqa: E501
    """Update existing Project

     # noqa: E501

    :param body: Project Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body: ModelProject = ModelProject.from_dict(connexion.request.get_json())  # noqa: E501
    exist_project = convert_project_to_dict_for_update(body)
    if not exist_project:
        return error_message(f"Project name is empty", 400), 400
    projects = Project.objects(id=exist_project["id"])
    if projects.count() < 1:
        return error_message(f"Project: {exist_project['id']} not found", 404, "Object not found"), 404
    project = projects[0]
    project.update(**exist_project)
    project.save()
    return 'OK'
