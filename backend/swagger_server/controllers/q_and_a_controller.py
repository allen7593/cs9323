import connexion
import six

from swagger_server.models.answer import Answer  # noqa: E501
from swagger_server.models.error import Error  # noqa: E501
from swagger_server import util
from swagger_server.qandaService import *


def get_q_and_a_answer(question):  # noqa: E501
    """Get Q and A answer

     # noqa: E501

    :param question: Question
    :type question: str

    :rtype: Answer
    """
    bot: QAndAService = get_bot()
    reply = bot.reply(question)
    return reply.split("<return>")
