import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.role import Role  # noqa: E501
from swagger_server.loggingService.Logger import Logger
from swagger_server.error import error_message
from swagger_server.databaseService.tables.Role import Role as DBRole
from swagger_server import util
from swagger_server.utils import convert_role_to_dict, convert_role_to_dict_for_create, convert_role_to_dict_for_update


def create_role(body):  # noqa: E501
    """Create new Role

     # noqa: E501

    :param body: Role Information
    :type body: dict | bytes

    :rtype: None
    """
    logger = Logger.get_logger()
    if connexion.request.is_json:
        body = Role.from_dict(connexion.request.get_json())  # noqa: E501
    new_role_dict = convert_role_to_dict_for_create(body)
    if DBRole.objects(role_name=new_role_dict["role_name"]).count() > 0:
        return error_message(f"Role: {new_role_dict['role_name']} has already exist", 400), 400
    new_role = DBRole(**new_role_dict)
    try:
        logger.info("Saving New Role")
        new_role.save()
        logger.info("New Role Saved")
    except Exception as e:
        logger.error(e)
        abort(400)
    return 'OK'


def get_roles():  # noqa: E501
    """Get all roles

     # noqa: E501


    :rtype: List[Role]
    """
    all_db_role = DBRole.objects()
    return [convert_role_to_dict(r) for r in all_db_role]


def update_role(body):  # noqa: E501
    """Update exsiting Role

     # noqa: E501

    :param body: Role Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Role.from_dict(connexion.request.get_json())  # noqa: E501
    exist_role = convert_role_to_dict_for_update(body)
    if not exist_role:
        return error_message(f"Role name is empty", 400), 400
    role = DBRole.objects.get(role_name=exist_role["role_name"])
    if not role:
        return error_message(f"Role: {exist_role['role_name']} not found", 404, "Object not found"), 404
    role.update(**exist_role)
    role.save()
    return 'OK'
