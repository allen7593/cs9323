import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.error import error_message
from swagger_server.loggingService.Logger import Logger
from swagger_server.models.team import Team as ModelTeam  # noqa: E501
from swagger_server.databaseService.tables.User import User as DBUser
from swagger_server import util
from swagger_server.databaseService.tables.Activity import Team as DBTeam
from swagger_server.utils import convert_db_team_to_dict, convert_team_to_dict_for_create, convert_team_to_dict_for_update, get_current_team
from swagger_server.utils import get_current_user


def create_team(body):  # noqa: E501
    """Create a new team

     # noqa: E501

    :param body: Create a new team
    :type body: dict | bytes

    :rtype: None
    """
    logger = Logger.get_logger()
    if connexion.request.is_json:
        body: ModelTeam = ModelTeam.from_dict(connexion.request.get_json())  # noqa: E501
    new_team_dict = convert_team_to_dict_for_create(body)
    if DBTeam.objects(name=new_team_dict["name"]).count() > 0:
        return error_message(f"Team: {new_team_dict['name']} has already exist", 400), 400
    new_team = DBTeam(**new_team_dict)
    try:
        logger.info("Saving New Team")
        new_team.save()
        logger.info("New Team Saved")
    except Exception as e:
        logger.error(e)
        return error_message(str(e), 400), 400
    return 'OK'


def get_all_teams():  # noqa: E501
    """Get All teams

     # noqa: E501


    :rtype: List[Team]
    """
    all_db_team = DBTeam.objects()
    return [ModelTeam.from_dict(convert_db_team_to_dict(t)) for t in all_db_team]


def get_team(team_id):  # noqa: E501
    """Get a team by ID

     # noqa: E501

    :param team_id: Team ID
    :type team_id: int

    :rtype: Team
    """
    team = DBTeam.objects(id=team_id)
    return ModelTeam.from_dict(convert_db_team_to_dict(team[0]))


def update_team(body):  # noqa: E501
    """Update team existing information

     # noqa: E501

    :param body: Update existiong team
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body: ModelTeam = ModelTeam.from_dict(connexion.request.get_json())  # noqa: E501
    exist_team = convert_team_to_dict_for_update(body)
    if not exist_team:
        return error_message(f"Team id is empty", 400), 400
    team = DBTeam.objects.get(id=exist_team["id"])
    if not team:
        return error_message(f"Team: {exist_team['id']} not found", 404, "Object not found"), 404
    team.update(**exist_team)
    team.save()
    return 'OK'


def get_team_current():
    """Get current team

    # noqa: E501

    :rtype: Team
    """
    team = get_current_team()
    if not team:
        return error_message(f"User is not in any team", 404, "Object not found"), 404
    return ModelTeam.from_dict(convert_db_team_to_dict(team))
