import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.error import error_message
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.term import Term as modelTerm  # noqa: E501
from swagger_server import util
from swagger_server.databaseService.tables.Term import Term as DBTerm
from swagger_server.utils.TermUtil import convert_term_to_dict, convert_term_to_dict_for_update, show_current_term


def create_term(body):  # noqa: E501
    """Create new term

     # noqa: E501

    :param body: Term Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = modelTerm.from_dict(connexion.request.get_json())  # noqa: E501
    new_term_dict = convert_term_to_dict(body)
    current_term_info: [DBTerm] = DBTerm.objects(term_name=body.term_name)
    if current_term_info.count() < 1:
        new_term = DBTerm(**new_term_dict)
        new_term.save()
        return 'ok'
    else:
        error_message("Duplicate Term Found", 400), 400
    # return 'do some magic!'


def get_terms():  # noqa: E501
    """Get all available term

     # noqa: E501

    :rtype: List[Term]
    """
    all_terms = DBTerm.objects()
    return [convert_term_to_dict(term) for term in all_terms]


def update_term(body):  # noqa: E501
    """Update exsiting term

     # noqa: E501

    :param body: Term Information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = modelTerm.from_dict(connexion.request.get_json())  # noqa: E501
    update_term_dict = convert_term_to_dict_for_update(body)
    current_term_info: [DBTerm] = DBTerm.objects(id=body.id)
    if current_term_info.count() < 1:
        abort(404)
    else:
        current_term_info[0].update(**update_term_dict)
        current_term_info[0].save()
        return 'ok'


def current_term():
    return convert_term_to_dict(show_current_term())
