import connexion
import six
from werkzeug.exceptions import abort

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.user import User as ModelUser  # noqa: E501
from swagger_server.databaseService.tables.User import User as DBUser
from swagger_server import util
from swagger_server.utils import convert_db_user_to_dict, convert_user_to_dict_for_update, UserUtil


def get_all_users():  # noqa: E501
    all_db_user = DBUser.objects()
    return [convert_db_user_to_dict(u) for u in all_db_user]


def get_user(user_id):  # noqa: E501
    """Get user by ID

     # noqa: E501

    :param user_id: user ID
    :type user_id: int

    :rtype: None
    """
    users: [DBUser] = DBUser.objects(id=user_id)
    if users.count() < 1:
        abort(404)
        return "User Not Found"
    return convert_db_user_to_dict(users[0])


def update_user(body):  # noqa: E501
    """Update user

     # noqa: E501

    :param body: User information
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body: ModelUser = ModelUser.from_dict(connexion.request.get_json())  # noqa: E501
    new_user_dict = convert_user_to_dict_for_update(body)
    current_user: [DBUser] = DBUser.objects(id=body.id)
    if current_user.count() < 1:
        abort(404)
    else:
        current_user[0].update(**new_user_dict)
        current_user[0].save()
        return 'OK'


def get_current_user():  # noqa: E501
    """Get Current User

     # noqa: E501


    :rtype: User
    """
    return convert_db_user_to_dict(UserUtil.get_current_user())
