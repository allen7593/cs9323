import connexion
import six
from bson import ObjectId

from swagger_server.databaseService.tables.VoteAndTicket import Vote
from swagger_server.error import error_message
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.ticket import Ticket  # noqa: E501
from swagger_server import util
from swagger_server.utils.VoteAndTicketUtil import model_ticket_to_db_ticket


def update_voting(vote_id, body=None):  # noqa: E501
    """voting project

     # noqa: E501

    :param vote_id: vote id
    :type vote_id: str
    :param body: voted
    :type body: list | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = [Ticket.from_dict(d) for d in connexion.request.get_json()]  # noqa: E501
    existing_votes = Vote.objects(id=vote_id)
    if existing_votes.count() < 1:
        return error_message("Vote Cannot be found", 404), 404
    vote = existing_votes[0]
    tickets = [model_ticket_to_db_ticket(t) for t in body]
    for ticket in tickets:
        ticket.save()
    new_tickets = vote.tickets + tickets
    # Error on reference, so change to OID
    ticketsid=[ObjectId(oid=t.id) for t in new_tickets ]
    vote.update(tickets=ticketsid)
    vote.save()
    return 'ok'
