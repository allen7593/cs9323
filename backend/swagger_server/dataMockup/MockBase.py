import os


class MockBase:
    def __init__(self):
        self.root = os.path.dirname(os.path.abspath(__file__))

    def get_path(self, path: str):
        return os.path.join(self.root, path).replace("\\", "/")

    def mock(self):
        pass
