from swagger_server.dataMockup.NotificationMockup import NotificationMockup
from swagger_server.dataMockup.PermissionMockupData import PermissionMockup
from swagger_server.dataMockup.ProjectMockupData import ProjectMocupData
from swagger_server.dataMockup.RoleMockupData import RoleMocupData
from swagger_server.dataMockup.TeamMocupData import TeamMocupData
from swagger_server.dataMockup.TermMockupData import TermMockUp
from swagger_server.dataMockup.UserMockupData import UserMockup


class MockUpService:
    def __init__(self, mock: bool):
        self.__mock: bool = mock
        self.__mock_services = list()

    def __call__(self, *args, **kwargs):
        if self.__mock:
            """
            Setup MockUp service
            """
            self.__mock_services.append(TermMockUp())
            self.__mock_services.append(PermissionMockup())
            self.__mock_services.append(RoleMocupData())
            self.__mock_services.append(UserMockup())
            self.__mock_services.append(ProjectMocupData())
            self.__mock_services.append(NotificationMockup())
            self.__mock_services.append(TeamMocupData())
            for mock_service in self.__mock_services:
                mock_service.mock()
