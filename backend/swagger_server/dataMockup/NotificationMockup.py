from datetime import datetime, timedelta

from swagger_server.dataMockup.MockBase import MockBase
from swagger_server.databaseService import Notification, Term, User, Role


class NotificationMockup(MockBase):
    def mock(self):
        mocked_objects = []
        mocked_objects.append(self.get_term_start_note())
        mocked_objects.append(self.get_term_end_notification())
        mocked_objects.append(self.get_assignment_notification())
        mocked_objects.append(self.get_remainder_notification())
        notes = Notification.objects()
        if notes.count() > 0:
            for item in mocked_objects:
                current = Notification.objects.get(name=item.name)
                if not current:
                    item.save()
                else:
                    current.update(release_date=item.release_date)
                    current.save()
        else:
            for item in mocked_objects:
                item.save()
        with open(self.get_path("resources/NotificationMockupData.csv"), "r") as reader:
            lines = reader.readlines()
            header_line = lines[0].replace("\r", "").replace("\n", "")
            headers = header_line.split(",")
            for i in range(1, len(lines)):
                current_line = lines[i].replace("\r", "").replace("\n", "")
                new_dict = self.__create_new_dict(headers)
                current = current_line.split("::")
                for j, item in enumerate(current):
                    new_dict[headers[j]] = item
                if Notification.objects(name=new_dict["name"]).count() < 1:
                    new_dict["term"] = Term.objects(term_name="2019T3")[0]
                    new_dict["created_date_time"] = str(datetime.now())
                    new_dict["updated_date_time"] = str(datetime.now())
                    new_dict["release_date"] = str(Term.objects.get(term_name="2019T3").start_date)
                    new_dict["instance_notification"] = False
                    new_dict["published"] = True
                    new_dict["notification_type"] = "Notification"
                    new_dict["target_roles"] = Role.objects
                    new_dict["pre_release_days"] = 5
                    new_dict["publisher"] = User.objects.get(email_address="tloveday0@symantec.com")
                    new_permission = Notification(**new_dict)
                    new_permission.save()

    def get_term_start_note(self) -> Notification:
        now = datetime.now()
        term: Term = Term.objects.get(term_name="2019T3")
        admin = User.objects.get(email_address="tloveday0@symantec.com")
        roles = Role.objects
        term_start_note = {
            "name": "Term Start",
            "description": "Term Start",
            "term": term,
            "created_date_time": str(now),
            "updated_date_time": str(now),
            "release_date": str(term.start_date),
            "pre_release_days": 5,
            "message": f"Welcome to COMP9323, The new term is start at {str(term.start_date)}, please keep tracking this website for more information",
            "publisher": admin,
            "instance_notification": False,
            "published": True,
            "colour_schema": "",
            "notification_type": "Notification",
            "target_roles": roles
        }
        term_start_note = Notification(**term_start_note)
        return term_start_note

    def get_term_end_notification(self) -> Notification:
        now = datetime.now()
        term: Term = Term.objects.get(term_name="2019T3")
        admin = User.objects.get(email_address="tloveday0@symantec.com")
        roles = Role.objects
        term_end_note = {
            "name": "Term End",
            "description": "Term End",
            "term": term,
            "created_date_time": str(now),
            "updated_date_time": str(now),
            "release_date": str(term.end_date),
            "pre_release_days": 5,
            "message": f"Welcome to COMP9323, The new term is end on {str(term.end_date)}, please keep tracking this website for more information",
            "publisher": admin,
            "instance_notification": False,
            "published": False,
            "colour_schema": "",
            "notification_type": "Notification",
            "target_roles": roles
        }
        term_end_note = Notification(**term_end_note)
        return term_end_note

    def get_assignment_notification(self) -> Notification:
        now = datetime.today()
        term: Term = Term.objects.get(term_name="2019T3")
        admin = User.objects.get(email_address="tloveday0@symantec.com")
        roles = Role.objects
        release_date = now + timedelta(days=5)
        assignment_due = {
            "name": "Assignment 1 Due",
            "description": "Assignment 1 Due",
            "term": term,
            "created_date_time": str(now),
            "updated_date_time": str(now),
            "release_date": str(now + timedelta(days=5)),
            "pre_release_days": 5,
            "message": f"Assignment is due on {release_date} days.",
            "publisher": admin,
            "instance_notification": False,
            "published": False,
            "colour_schema": "",
            "notification_type": "Assignment Due",
            "target_roles": roles
        }
        assignment_due = Notification(**assignment_due)
        return assignment_due

    def get_remainder_notification(self) -> Notification:
        now = datetime.today()
        term: Term = Term.objects.get(term_name="2019T3")
        admin = User.objects.get(email_address="tloveday0@symantec.com")
        roles = Role.objects
        remainder = {
            "name": "Assignment Tips",
            "description": "Assignment Tips",
            "term": term,
            "created_date_time": str(now),
            "updated_date_time": str(now),
            "release_date": str(now + timedelta(days=5)),
            "pre_release_days": 0,
            "message": f"Tips: No code no bugs",
            "publisher": admin,
            "instance_notification": False,
            "published": False,
            "colour_schema": "",
            "notification_type": "Remainder",
            "target_roles": roles
        }
        remainder = Notification(**remainder)
        return remainder

    def __create_new_dict(self, headers: list) -> dict:
        new_dict = dict()
        for header in headers:
            new_dict[header] = None
        return new_dict