from swagger_server.dataMockup.MockBase import MockBase
from swagger_server.databaseService import Permission, Role


class RoleMocupData(MockBase):
    def mock(self):
        created_date_time = "20/05/2019"
        updated_date_time = "20/05/2019"

        # Add Role Admin
        admin_dict = dict()
        admin_dict["created_date_time"] = created_date_time
        admin_dict["updated_date_time"] = updated_date_time
        admin_dict["role_name"] = "admin"
        admin_dict["description"] = "admin"
        admin_permission = Permission.objects(domain="*", action="*")
        admin_dict["permissions"] = [admin_permission[0]]

        if Role.objects(role_name="admin").count() < 1:
            admin_role = Role(**admin_dict)
            admin_role.save()
        admin_dict.clear()

        # Add Mentor Role
        mentor_dict = dict()
        mentor_dict["created_date_time"] = created_date_time
        mentor_dict["updated_date_time"] = updated_date_time
        mentor_dict["role_name"] = "mentor"
        mentor_dict["description"] = "mentor"
        mentor_dict["permissions"] = list()

        mentor_dict["permissions"].append(Permission.objects(domain="team", action="edit")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="team", action="delete")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="team", action="search")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="team", action="assign_user")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="team", action="edit_member")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="team", action="edit_project")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="project", action="view")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="project", action="edit")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="notification", action="*")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="user", action="view")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="user", action="self_edit")[0])
        mentor_dict["permissions"].append(Permission.objects(domain="term", action="view")[0])

        if Role.objects(role_name="mentor").count() < 1:
            student_role = Role(**mentor_dict)
            student_role.save()
        mentor_dict.clear()

        # Add Student Role
        student_dict = dict()
        student_dict["created_date_time"] = created_date_time
        student_dict["updated_date_time"] = updated_date_time
        student_dict["role_name"] = "student"
        student_dict["description"] = "student"
        student_dict["permissions"] = list()

        student_dict["permissions"].append(Permission.objects(domain="team", action="create")[0])
        student_dict["permissions"].append(Permission.objects(domain="team", action="edit")[0])
        student_dict["permissions"].append(Permission.objects(domain="team", action="delete")[0])
        student_dict["permissions"].append(Permission.objects(domain="team", action="search")[0])
        student_dict["permissions"].append(Permission.objects(domain="team", action="edit_member")[0])
        student_dict["permissions"].append(Permission.objects(domain="project", action="view")[0])
        student_dict["permissions"].append(Permission.objects(domain="notification", action="view")[0])
        student_dict["permissions"].append(Permission.objects(domain="user", action="view")[0])
        student_dict["permissions"].append(Permission.objects(domain="user", action="self_edit")[0])
        student_dict["permissions"].append(Permission.objects(domain="term", action="view")[0])

        if Role.objects(role_name="student").count() < 1:
            student_role = Role(**student_dict)
            student_role.save()
        student_dict.clear()
