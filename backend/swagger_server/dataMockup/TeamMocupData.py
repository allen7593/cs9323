from random import randint

from mongoengine import QuerySet

from swagger_server.dataMockup.MockBase import MockBase
from swagger_server.databaseService import Team, Term, User, Vote, Project


class TeamMocupData(MockBase):
    def mock(self):
        with open(self.get_path("resources/TeamMockup.csv"), "r") as reader:
            lines = reader.readlines()
            header_line = lines[0].replace("\r", "").replace("\n", "")
            headers = header_line.split(",")
            ex_user_list = list()
            for i in range(1, len(lines)):
                current_line = lines[i].replace("\r", "").replace("\n", "")
                new_dict = self.__create_new_dict(headers)
                current = current_line.split("::")
                for j, item in enumerate(current):
                    new_dict[headers[j]] = item
                if Team.objects(name=new_dict["name"]).count() < 1:
                    new_vote = Vote()
                    new_vote.save()
                    new_dict["term"] = Term.objects.get(term_name="2019T3")
                    new_dict["vote"] = new_vote
                    new_dict["team_members"] = self.get_random_objects(User.objects(), ex_user_list, 5)
                    new_dict["projects"] = self.get_random_objects_for_project(Project.objects(), [], 6)
                    new_team = Team(**new_dict)
                    new_team.save()

    def get_random_objects(self, objects: QuerySet, pre_ex_list: list, num):
        ex_list = pre_ex_list
        result_list = []
        query_list = []
        for o in objects:
            if o.roles[0].role_name == "student":
                query_list.append(str(o.id))
        team_leader_set = False
        is_team = False
        while len(result_list) < num:
            rand = randint(0, len(query_list) - 1)
            if query_list[rand] not in ex_list:
                ex_list.append(query_list[rand])
                if isinstance(objects.with_id(query_list[rand]), User) and not team_leader_set:
                    team_leader_set = True
                    leader: User = objects.with_id(query_list[rand])
                    leader.is_team_leader = True
                    leader.save()
                result_list.append(objects.with_id(query_list[rand]))
        return result_list

    def get_random_objects_for_project(self, objects: QuerySet, pre_ex_list: list, num):
        ex_list = pre_ex_list
        result_list = []
        query_list = [str(o.id) for o in objects]
        team_leader_set = False
        is_team = False
        while len(result_list) < num:
            rand = randint(0, len(query_list) - 1)
            if query_list[rand] not in ex_list:
                ex_list.append(query_list[rand])
                if isinstance(objects.with_id(query_list[rand]), User) and not team_leader_set:
                    team_leader_set = True
                    leader: User = objects.with_id(query_list[rand])
                    leader.is_team_leader = True
                    leader.save()
                result_list.append(objects.with_id(query_list[rand]))
        return result_list

    def __create_new_dict(self, headers: list) -> dict:
        new_dict = dict()
        for header in headers:
            new_dict[header] = None
        return new_dict
