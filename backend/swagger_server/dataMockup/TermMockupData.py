from swagger_server.dataMockup.MockBase import MockBase
from swagger_server.databaseService import Term


class TermMockUp(MockBase):
    def mock(self):
        with open(self.get_path("resources/TermMockupData.csv"), "r") as reader:
            lines = reader.readlines()
            header_line = lines[0].replace("\r", "").replace("\n", "")
            headers = header_line.split(",")
            for i in range(1, len(lines)):
                current_line = lines[i].replace("\r", "").replace("\n", "")
                new_dict = self.__create_new_dict(headers)
                current = current_line.split("::")
                for j, item in enumerate(current):
                    new_dict[headers[j]] = item
                if Term.objects(term_name=new_dict["term_name"]).count() < 1:
                    new_term = Term(**new_dict)
                    new_term.save()

    def __create_new_dict(self, headers: list) -> dict:
        new_dict = dict()
        for header in headers:
            new_dict[header] = None
        return new_dict
