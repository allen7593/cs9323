from swagger_server.dataMockup.MockBase import MockBase
from swagger_server.databaseService import User, Role, Term


class UserMockup(MockBase):
    def mock(self):
        with open(self.get_path("resources/PersonMockup.csv"), "r") as reader:
            admin_count = 1
            mentor_count = 10
            lines = reader.readlines()
            header_line = lines[0].replace("\r", "").replace("\n", "")
            headers = header_line.split(",")
            for i in range(1, len(lines)):
                current_line = lines[i].replace("\r", "").replace("\n", "")
                new_dict = self.__create_new_dict(headers)
                current = current_line.split("::")
                for j, item in enumerate(current):
                    new_dict[headers[j]] = item
                if User.objects(first_name=new_dict["first_name"], middle_name=new_dict["middle_name"], last_name=new_dict["last_name"]).count() < 1:
                    new_dict["term"] = Term.objects(term_name="2019T3")[0]
                    if admin_count > 0:
                        new_dict["roles"] = Role.objects(role_name="admin")
                        admin_count -= 1
                    elif mentor_count > 0:
                        new_dict["roles"] = Role.objects(role_name="mentor")
                        mentor_count -= 1
                    else:
                        new_dict["roles"] = Role.objects(role_name="student")
                    new_user = User(**new_dict)
                    new_user.save()

    def __create_new_dict(self, headers: list) -> dict:
        new_dict = dict()
        for header in headers:
            new_dict[header] = None
        return new_dict