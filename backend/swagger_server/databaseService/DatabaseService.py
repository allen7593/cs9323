from mongoengine import *

from swagger_server.loggingService.Logger import Logger


class DatabaseService:
    __da_config: dict
    __db_host: str
    __db_port: int
    __db_name: str = "comp9323"

    def __init__(self, database_config):
        self.__logger = Logger.get_logger()
        self.__logger.info("Start Configuring Database Service")
        self.__dereference_config(database_config)
        self.__db_client = connect(self.__db_name, host=self.__db_host, port=self.__db_port)

    def __dereference_config(self, database_config: dict):
        self.__logger.info("De-referencing database config")
        self.__db_host = database_config["host"]
        self.__logger.info(f"Host name: {self.__db_host}")
        self.__db_port = database_config["port"]
        self.__logger.info(f"Port Number: {self.__db_port}")
