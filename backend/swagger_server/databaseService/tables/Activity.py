from mongoengine import *

from swagger_server.databaseService.tables.Project import Project
from swagger_server.databaseService.tables.VoteAndTicket import Vote
from swagger_server.databaseService.tables.User import User
from .Role import Role
from .Term import Term

NOTIFICATION_TYPE = (
    "Assignment Due",
    "Notification",
    "Remainder"
)


class Activity(Document):
    name = StringField(max_length=50, required=True)
    description = StringField(max_length=300)
    meta = {'allow_inheritance': True}
    term = ReferenceField(Term, reverse_delete_rule=CASCADE)
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()


class Team(Activity):
    projects = ListField(ReferenceField(Project))
    team_members = ListField(ReferenceField(User))
    assignee = ReferenceField(User)
    vote = ReferenceField(Vote)


class Assignment(Activity):
    project = ListField(ReferenceField(Project))


class KeyDate(Activity):
    assignment = ReferenceField(Assignment)
    date = DateTimeField()
    message = StringField(max_length=300)


class Notification(Activity):
    release_date = DateTimeField()
    pre_release_days = IntField(default=0)
    message = StringField()
    publisher = ReferenceField(User)
    instance_notification = BooleanField()
    published = BooleanField()
    colour_schema = StringField()
    notification_type = StringField(choices=NOTIFICATION_TYPE)
    target_roles = ListField(ReferenceField(Role))


class QAndA(Activity):
    question_format = StringField()
    answer = StringField()
    other_actions = StringField()
