from mongoengine import *


class Permission(Document):
    domain = StringField(required=True)
    action = StringField(required=True)
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()