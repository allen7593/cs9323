from mongoengine import *

from swagger_server.databaseService.tables.Term import Term


class Project(Document):
    name = StringField(max_length=50, required=True)
    description = StringField(max_length=300)
    term = ReferenceField(Term, reverse_delete_rule=CASCADE)
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()
    max_number = IntField()
