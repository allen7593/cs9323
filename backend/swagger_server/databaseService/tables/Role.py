from mongoengine import *

from .Permission import Permission


class Role(Document):
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()
    role_name = StringField(max_length=50, required=True)
    description = StringField(max_length=300)
    permissions = ListField(ReferenceField(Permission), reverse_delete_rule=CASCADE)