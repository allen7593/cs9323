from mongoengine import *

from swagger_server.databaseService import User


class Session(Document):
    session_id = StringField(required=True)
    user = ReferenceField(User, required=True)
