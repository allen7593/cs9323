from mongoengine import *


class Term(Document):
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()
    term_name = StringField(max_length=50, required=True)
    description = StringField(max_length=300)
    start_date = DateTimeField()
    end_date = DateTimeField()
