from mongoengine import *

from swagger_server.databaseService.tables.User import User
from swagger_server.databaseService.tables.Activity import Project


class Ticket(Document):
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()
    user = ReferenceField(User)
    project = ReferenceField(Project)
