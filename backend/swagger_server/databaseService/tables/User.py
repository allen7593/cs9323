from mongoengine import *

from .Role import Role
from .Term import Term


class User(Document):
    first_name = StringField(max_length=50)
    middle_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    dob = DateField()
    email_address = EmailField(required=True)
    gender = StringField()
    password = StringField()
    created_date_time = DateTimeField()
    updated_date_time = DateTimeField()
    roles = ListField(ReferenceField(Role, reverse_delete_rule=CASCADE))
    description = StringField(max_length=300)
    term = ReferenceField(Term, reverse_delete_rule=CASCADE)
    is_team_leader = BooleanField()
