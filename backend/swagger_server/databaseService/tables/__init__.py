from swagger_server.databaseService.tables.User import User
from swagger_server.databaseService.tables.Permission import Permission
from swagger_server.databaseService.tables.Role import Role
from swagger_server.databaseService.tables.Term import Term
from swagger_server.databaseService.tables.Project import Project
from swagger_server.databaseService.tables.Activity import *
from swagger_server.databaseService.tables.VoteAndTicket import *
