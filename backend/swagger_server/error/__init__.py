def error_message(details: str, status: int, title: str = "Bad Request"):
    return {
        "detail": details,
        "status": status,
        "title": title,
        "type": "about:blank"
    }
