import time

import jwt

from swagger_server.databaseService import User
from swagger_server.loggingService.Logger import Logger
from swagger_server.utils.RoleUtil import convert_role_to_dict

JWT_ISSUER = 'comp9323'
JWT_SECRET = 'secret'
JWT_LIFETIME_SECONDS = 60000
JWT_ALGORITHM = 'HS256'


def _current_timestamp() -> int:
    return int(time.time())


def encode_token(user: User):
    timestamp = _current_timestamp()
    payload = {
        "iss": JWT_ISSUER,
        "iat": int(timestamp),
        "exp": int(timestamp + JWT_LIFETIME_SECONDS),
        "sub": str(user.email_address),
        "user_id": str(user.id),
        "roles": [convert_role_to_dict(r) for r in user.roles]
    }
    return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)


def decode_token(token):
    logger = Logger.get_logger()
    logger.debug("Authorizing")
    decoded_token = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    logger.debug(f"User: {decoded_token['sub']}")
    return decoded_token
