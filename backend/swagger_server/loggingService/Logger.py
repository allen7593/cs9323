import logging
from logging import RootLogger

logger = None


def set_logger(external_logger):
    global logger
    logger = external_logger

# Calling this in __init__ or __new__ if you want to use it in class
# Calling this at the beginning of the method
class Logger:
    global logger

    @staticmethod
    def get_logger():
        if logger:
            return logger
