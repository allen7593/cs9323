from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from swagger_server.loggingService.Logger import Logger


class MailMessage:
    def __init__(self, from_user):
        self.__logger = Logger.get_logger()
        self.__message = MIMEMultipart("alternative")
        self.__from_user = from_user
        self.__message["From"] = self.__from_user
        self.__meta_data_set = False

    def set_message_meta_data(self, subject="Default Subject", to="Default Receiver"):
        self.__logger.info("Setting Meta Data for Message")
        self.__message["Subject"] = subject
        self.__logger.info(f"Subject: {subject}")
        self.__message["To"] = to
        self.__logger.info(f"Receiver: {to}")
        self.__meta_data_set = True

    def add_plain_text(self, plain_text):
        self.__logger.debug(f"Adding new Plain Text: {plain_text}")
        part = MIMEText(plain_text, "plain")
        self.__message.attach(part)

    def add_plain_text_template(self, template: str, **kwargs):
        new_message = self.__format_message(template, **kwargs)
        self.__logger.debug(f"Adding new Plain Text: {new_message}")
        part = MIMEText(new_message, "plain")
        self.__message.attach(part)

    def add_html(self, html):
        self.__logger.debug(f"Adding new HTML Text: {html}")
        part = MIMEText(html, "html")
        self.__message.attach(part)

    def add_html_template(self, template: str, **kwargs):
        new_message = self.__format_message(template, **kwargs)
        self.__logger.debug(f"Adding new HTML Text: {new_message}")
        part = MIMEText(new_message, "html")
        self.__message.attach(part)

    def __format_message(self, template, **kwargs):
        return template.format(**kwargs)

    def __str__(self):
        if not self.__meta_data_set:
            self.__logger.error(f"Meta Data not set")
            raise Exception("Message Meta data not set")
        return self.__message.as_string()
