import smtplib
import ssl

from swagger_server.loggingService.Logger import Logger
from .MailMessage import MailMessage


class MailService:
    __config: dict
    __smtp_server: str
    __smtp_port: int
    __user: str
    __password: str
    __server: smtplib.SMTP = None
    __context: ssl.SSLContext

    def __init__(self, config: dict):
        self.__config = config
        if not config:
            raise Exception("No mail config provided")
        self.__context = ssl.create_default_context()
        self.__reload_config(config)

    def connect(self):
        if self.__server is None:
            self.__reload_config(self.__config)
        self.__server.ehlo_or_helo_if_needed()
        self.__server.login(self.__user, self.__password)

    def close(self):
        self.__server.quit()
        self.__server = None

    def __reload_config(self, config):
        self.__config = config
        self.__dereference_config(config)
        # if self.__server is not None:
        #     self.__server.quit()
        self.__server = smtplib.SMTP(self.__smtp_server, self.__smtp_port)
        self.__server.connect(self.__smtp_server, self.__smtp_port)
        self.__server.ehlo_or_helo_if_needed()
        self.__server.starttls(context=self.__context)
        # self.__server.ehlo_or_helo_if_needed()

    def __dereference_config(self, config):
        self.__smtp_server = config["smtp_server"]
        self.__smtp_port = config["port"]
        self.__user = config["login"]
        self.__password = config["password"]

    def create_new_message(self):
        logger = Logger.get_logger()
        logger.info("Creating New Email Message")
        return MailMessage(self.__user)

    def send_email(self, to: str, message: MailMessage):
        logger = Logger.get_logger()
        logger.info(f"Sending email to: {to}\nWith Content: {str(message)}")
        try:
            self.__server.sendmail(from_addr=self.__user, to_addrs=to, msg=str(message))
        except Exception as e:
            self.__reload_config(self.__config)
            self.__server.sendmail(from_addr=self.__user, to_addrs=to, msg=str(message))
            logger.error(e)
        except smtplib.SMTPServerDisconnected as e:
            self.__reload_config(self.__config)
            self.__server.sendmail(from_addr=self.__user, to_addrs=to, msg=str(message))
            logger.error(e)
