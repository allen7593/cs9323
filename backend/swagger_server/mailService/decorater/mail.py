import inspect

from ..MailService import MailService

mail_service = None
mail_config: dict = None

def set_mail_config(mc):
    global mail_config
    mail_config = mc

def set_mail_service(ms):
    global mail_service
    mail_service = ms

def mail(arg):
    global mail_service
    global mail_config

    if mail_service is None and mail_config is None:
        return

    if mail_service is None:
        ## init mail service
        mail_service = MailService(mail_config)

    if inspect.isfunction(arg):
        def wrapper(*args, **kwargs):
            kwargs["mail_service"] = mail_service
            out = arg(*args, **kwargs)
            return out
        return wrapper
    elif inspect.isclass(arg):
        class NewCls:
            def __init__(self, *args, **kwargs):
                self.new_instance = arg(*args, **kwargs)
                try:
                    getattr(self.new_instance, "mail_service")
                except AttributeError:
                    raise AttributeError(f"Variable mail_service not found in class {arg.__name__} definition")
                setattr(self.new_instance, "mail_service", mail_service)

            def __getattr__(self, item):
                return getattr(self.new_instance, item)

        return NewCls
    else:
        raise Exception(f"Type of {arg.__name__} not supported by mail decorator")
