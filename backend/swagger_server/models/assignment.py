# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.project import Project  # noqa: F401,E501
from swagger_server.models.term import Term  # noqa: F401,E501
from swagger_server import util


class Assignment(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, id: str=None, name: str=None, term: Term=None, description: str=None, created_date_time: datetime=None, updated_date_time: datetime=None, project: Project=None):  # noqa: E501
        """Assignment - a model defined in Swagger

        :param id: The id of this Assignment.  # noqa: E501
        :type id: str
        :param name: The name of this Assignment.  # noqa: E501
        :type name: str
        :param term: The term of this Assignment.  # noqa: E501
        :type term: Term
        :param description: The description of this Assignment.  # noqa: E501
        :type description: str
        :param created_date_time: The created_date_time of this Assignment.  # noqa: E501
        :type created_date_time: datetime
        :param updated_date_time: The updated_date_time of this Assignment.  # noqa: E501
        :type updated_date_time: datetime
        :param project: The project of this Assignment.  # noqa: E501
        :type project: Project
        """
        self.swagger_types = {
            'id': str,
            'name': str,
            'term': Term,
            'description': str,
            'created_date_time': datetime,
            'updated_date_time': datetime,
            'project': Project
        }

        self.attribute_map = {
            'id': 'id',
            'name': 'name',
            'term': 'term',
            'description': 'description',
            'created_date_time': 'created_date_time',
            'updated_date_time': 'updated_date_time',
            'project': 'project'
        }
        self._id = id
        self._name = name
        self._term = term
        self._description = description
        self._created_date_time = created_date_time
        self._updated_date_time = updated_date_time
        self._project = project

    @classmethod
    def from_dict(cls, dikt) -> 'Assignment':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The assignment of this Assignment.  # noqa: E501
        :rtype: Assignment
        """
        return util.deserialize_model(dikt, cls)

    @property
    def id(self) -> str:
        """Gets the id of this Assignment.


        :return: The id of this Assignment.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id: str):
        """Sets the id of this Assignment.


        :param id: The id of this Assignment.
        :type id: str
        """

        self._id = id

    @property
    def name(self) -> str:
        """Gets the name of this Assignment.


        :return: The name of this Assignment.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this Assignment.


        :param name: The name of this Assignment.
        :type name: str
        """

        self._name = name

    @property
    def term(self) -> Term:
        """Gets the term of this Assignment.


        :return: The term of this Assignment.
        :rtype: Term
        """
        return self._term

    @term.setter
    def term(self, term: Term):
        """Sets the term of this Assignment.


        :param term: The term of this Assignment.
        :type term: Term
        """

        self._term = term

    @property
    def description(self) -> str:
        """Gets the description of this Assignment.


        :return: The description of this Assignment.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this Assignment.


        :param description: The description of this Assignment.
        :type description: str
        """

        self._description = description

    @property
    def created_date_time(self) -> datetime:
        """Gets the created_date_time of this Assignment.


        :return: The created_date_time of this Assignment.
        :rtype: datetime
        """
        return self._created_date_time

    @created_date_time.setter
    def created_date_time(self, created_date_time: datetime):
        """Sets the created_date_time of this Assignment.


        :param created_date_time: The created_date_time of this Assignment.
        :type created_date_time: datetime
        """

        self._created_date_time = created_date_time

    @property
    def updated_date_time(self) -> datetime:
        """Gets the updated_date_time of this Assignment.


        :return: The updated_date_time of this Assignment.
        :rtype: datetime
        """
        return self._updated_date_time

    @updated_date_time.setter
    def updated_date_time(self, updated_date_time: datetime):
        """Sets the updated_date_time of this Assignment.


        :param updated_date_time: The updated_date_time of this Assignment.
        :type updated_date_time: datetime
        """

        self._updated_date_time = updated_date_time

    @property
    def project(self) -> Project:
        """Gets the project of this Assignment.


        :return: The project of this Assignment.
        :rtype: Project
        """
        return self._project

    @project.setter
    def project(self, project: Project):
        """Sets the project of this Assignment.


        :param project: The project of this Assignment.
        :type project: Project
        """

        self._project = project
