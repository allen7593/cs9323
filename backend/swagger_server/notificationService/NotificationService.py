import os
import threading
import time
from datetime import timedelta, datetime

import schedule

from swagger_server.databaseService import Notification, User
from swagger_server.loggingService.Logger import Logger
from swagger_server.mailService import MailService, MailMessage
from swagger_server.mailService.decorater import mail

available_email_list = [
    "z5185341@ad.unsw.edu.au"
]


class NotificationService:
    @staticmethod
    def create_email_template(notification: Notification, user: User) -> dict:
        note_type = notification.notification_type
        if note_type == "Assignment Due":
            return {
                "user_name": f"{user.first_name}",
                "assignment_name": notification.name,
                "assignment_due_date": str(notification.release_date),
                "days_left": notification.pre_release_days,
                "publisher_name": f"{notification.publisher.first_name} {notification.publisher.last_name}"
            }
        else:
            return {
                "user_name": user.first_name,
                "message": notification.message,
                "publisher_name": f"{notification.publisher.first_name} {notification.publisher.last_name}"
            }

    @staticmethod
    def get_email_template_by_type(notification: Notification) -> str:
        note_type = notification.notification_type
        if note_type == "Assignment Due":
            with open(NotificationService.get_path("templates/AssignmentDueTemplate.html"), "r") as reader:
                lines = reader.readlines()
                return "\n".join(lines)
        else:
            with open(NotificationService.get_path("templates/NotificationTemplate.html"), "r") as reader:
                lines = reader.readlines()
                return "\n".join(lines)

    @staticmethod
    def start(interval):
        logger = Logger.get_logger()
        logger.info("Starting Notification Scheduler")
        schedule.every(30).minutes.do(NotificationService.schedule_job)
        logger.info("Set to interval to 30 mins")
        cease_continuous_run = threading.Event()

        class ScheduleThread(threading.Thread):
            @classmethod
            def run(cls):
                while not cease_continuous_run.is_set():
                    schedule.run_pending()
                    time.sleep(interval)

        continuous_thread = ScheduleThread()
        continuous_thread.start()
        return cease_continuous_run

    @staticmethod
    @mail
    def publish(notification: Notification, mail_service: MailService = None):
        logger = Logger.get_logger()
        email_template = NotificationService.get_email_template_by_type(notification)
        logger.info(f"Email Template: {email_template}")
        target_roles = notification.target_roles
        logger.info(f"Email Template: {', '.join([role.role_name for role in target_roles])}")
        users: [User] = User.objects()
        mail_service.connect()
        for user in users:
            if user.email_address in available_email_list:
                for role in user.roles:
                    if role.role_name in [role.role_name for role in target_roles]:
                        logger.info(f"Publish to: {user.email_address}")
                        new_message: MailMessage = mail_service.create_new_message()
                        message: dict = NotificationService.create_email_template(notification, user)
                        new_message.set_message_meta_data("Comp9323 Notification Service", user.email_address)
                        new_message.add_html_template(email_template, **message)
                        mail_service.send_email(user.email_address, new_message)
                        logger.info(f"Send email success")
                        break
        mail_service.close()
        days_remain = notification.pre_release_days
        if not notification.instance_notification:
            notification.update(published=True, pre_release_days=days_remain - 1)
        else:
            notification.update(published=True, pre_release_days=0)
        notification.save()

    @staticmethod
    def schedule_job():
        logger = Logger.get_logger()
        logger.info(f"Start Scheduler: {str(datetime.now())}")
        notifications = Notification.objects(instance_notification=False)
        for notification in notifications:
            if NotificationService.check_publish_status(notification):
                NotificationService.publish(notification)

    @staticmethod
    def check_publish_status(notification: Notification):
        today = datetime.now().replace(hour=0, minute=0, second=0)
        now = datetime.now()
        release_date = notification.release_date
        pre_release_days = notification.pre_release_days
        if today < (release_date + timedelta(days=-pre_release_days)) < now and pre_release_days > -1:
            return True
        else:
            return False

    @staticmethod
    def get_path(path: str):
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), path).replace("\\", "/")
