import os

from rivescript import RiveScript

from swagger_server.utils import get_current_user

bot = None


class QAndAService:
    def __init__(self):
        self.bot = RiveScript({"debug": True})
        self.script_path = self.path("script")
        self.bot.load_directory(self.script_path)
        self.bot.sort_replies()

    def path(self, path: str):
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), path).replace("\\", "/")

    def reply(self, message: str):
        current_user = get_current_user()
        return self.bot.reply(current_user.email_address, message)


def init_bot():
    global bot
    bot = QAndAService()


def get_bot() -> QAndAService:
    global bot
    return bot
