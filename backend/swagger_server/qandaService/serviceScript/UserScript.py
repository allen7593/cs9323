import datetime

from swagger_server.databaseService import User, Notification, Role, Activity
from swagger_server.mailService import MailService
from swagger_server.mailService.decorater import mail
from swagger_server.notificationService.NotificationService import available_email_list
from swagger_server.utils import UserUtil, PermissionUtils


def get_user_by_role(role: str):
    users = User.objects()
    user_list = []
    for user in users:
        user_roles = user.roles
        if role in [r.role_name for r in user_roles]:
            user_list.append(f"{user.first_name} {user.last_name.upper()}: {user.email_address}")

    return "<return>".join(user_list)


def validate_notification_create() -> bool:
    return PermissionUtils.validate_permission("notification", "create")


@mail
def send_notification(args, mail_service: MailService = None):
    current_str = " ".join(args)
    parms = current_str.split("#=#")
    role_str = parms[0]
    subject_str = parms[1]
    message_str = parms[2]
    roles = role_str.split(" ")
    current_user = UserUtil.get_current_user()
    users = User.objects()
    mail_template = """
            <p>
                Dear {target_name}:<br/>
                {message}<br/>
                <br/>
                Regards,<br/>
                {name}<br/>
                -- This is a bot generated message, please do not reply.<br/>
            </p>
        """
    new_notification = {
        "name": subject_str,
        "description": "This is a bot generated message",
        "created_date_time": str(datetime.datetime.now().isoformat()),
        "updated_date_time": str(datetime.datetime.now().isoformat()),
        "release_date": str(datetime.datetime.now().isoformat()),
        "pre_release_days": 0,
        "message": message_str,
        "publisher": current_user,
        "instance_notification": True,
        "published": False,
        "colour_schema": "fake colour",
        "notification_type": "Notification",
        "target_roles": Role.objects(role_name__in=roles),
        "term": current_user.term
    }
    notification_object = Notification(**new_notification)
    notification_object.save()

    try:
        mail_service.connect()
    except Exception as e:
        return str(e)
    for user in users:
        user_roles = user.roles
        if user.email_address in available_email_list:
            for role in roles:
                if role in [r.role_name for r in user_roles]:
                    new_message = mail_service.create_new_message()
                    new_message.set_message_meta_data(subject_str, user.email_address)
                    message_dict = {
                        "target_name": user.first_name,
                        "message": message_str,
                        "name": f"{current_user.first_name} {current_user.last_name}"
                    }
                    new_message.add_html_template(mail_template, **message_dict)
                    mail_service.send_email(user.email_address, new_message)
    mail_service.close()
    notification_object.update(published=True)
    notification_object.save()


@mail
def send_mail_to_all(question: list, mail_service: MailService = None):
    current_user = UserUtil.get_current_user()
    users = User.objects()
    mail_template = """
        <p>
            Dear {target_name}:<br/>
            Here is the question from <b>student</b>: {name}, <b>email</b>: {email_address}:<br/>
            <b>Question</b>: {question}<br/>
            <br/>
            Regards,<br/>
            {name}<br/>
            -- This is a system generated message, please do not reply.<br/>
        </p>
    """
    try:
        mail_service.connect()
    except Exception as e:
        return str(e)
    for user in users:
        user_roles = user.roles
        if user.email_address in available_email_list:
            if "admin" in [r.role_name for r in user_roles] or "mentor" in [r.role_name for r in user_roles]:
                new_message = mail_service.create_new_message()
                new_message.set_message_meta_data("Question from student", user.email_address)
                message_dict = {
                    "target_name": user.first_name,
                    "name": f"{current_user.first_name} {current_user.last_name}",
                    "email_address": current_user.email_address,
                    "question": " ".join(question)
                }
                new_message.add_html_template(mail_template, **message_dict)
                mail_service.send_email(user.email_address, new_message)
    mail_service.close()
