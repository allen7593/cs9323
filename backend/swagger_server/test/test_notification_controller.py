# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.notification import Notification  # noqa: E501
from swagger_server.models.notifications import Notifications  # noqa: E501
from swagger_server.test import BaseTestCase


class TestNotificationController(BaseTestCase):
    """NotificationController integration test stubs"""

    def test_create_notification(self):
        """Test case for create_notification

        Create New Notification
        """
        body = Notification()
        response = self.client.open(
            '//notifications',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_notifications(self):
        """Test case for get_notifications

        Get User Notifications
        """
        response = self.client.open(
            '//notifications',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
