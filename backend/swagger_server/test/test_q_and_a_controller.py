# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.answer import Answer  # noqa: E501
from swagger_server.models.error import Error  # noqa: E501
from swagger_server.test import BaseTestCase


class TestQAndAController(BaseTestCase):
    """QAndAController integration test stubs"""

    def test_get_q_and_a_answer(self):
        """Test case for get_q_and_a_answer

        Get Q and A answer
        """
        response = self.client.open(
            '//qanda/{question}'.format(question='question_example'),
            method='PUT')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
