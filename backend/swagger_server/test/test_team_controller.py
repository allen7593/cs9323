# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.team import Team  # noqa: E501
from swagger_server.test import BaseTestCase


class TestTeamController(BaseTestCase):
    """TeamController integration test stubs"""

    def test_create_team(self):
        """Test case for create_team

        Create a new team
        """
        body = Team()
        response = self.client.open(
            '//team',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_all_teams(self):
        """Test case for get_all_teams

        Get All teams
        """
        response = self.client.open(
            '//team',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_team(self):
        """Test case for get_team

        Get a team by ID
        """
        response = self.client.open(
            '//team/{team_id}'.format(team_id=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_team(self):
        """Test case for update_team

        Update team existing information
        """
        body = Team()
        response = self.client.open(
            '//team',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
