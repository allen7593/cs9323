import datetime

from swagger_server.databaseService import User, Term
from swagger_server.utils import convert_term_to_dict, convert_db_user_to_dict, convert_role_to_dict, get_user, \
    get_current_user
from swagger_server.utils.RoleUtil import get_role


def convert_notification_to_dict(notification):
    return {
        "name": notification.name,
        "description": notification.description,
        "term": convert_term_to_dict(notification.term),
        "created_date_time": str(notification.created_date_time),
        "updated_date_time": str(notification.updated_date_time),
        "release_date": str(notification.release_date),
        "pre_release_days": notification.pre_release_days,
        "message": notification.message,
        "publisher": convert_db_user_to_dict(notification.publisher),
        "instance_notification": notification.instance_notification,
        "published": notification.published,
        "colour_schema": notification.colour_schema,
        "notification_type": notification.notification_type,
        "target_roles": [convert_role_to_dict(r) for r in notification.target_roles]
    }


def convert_notification_to_dict_for_create(notification):
    return {
        "name": notification.name,
        "description": notification.description,
        "term": get_current_user().term,
        "created_date_time": str(datetime.datetime.now().isoformat()),
        "updated_date_time": str(datetime.datetime.now().isoformat()),
        "release_date": str(notification.release_date),
        "pre_release_days": notification.pre_release_days,
        "message": notification.message,
        "publisher": get_current_user(),
        "instance_notification": notification.instance_notification,
        "published": notification.published,
        "colour_schema": notification.colour_schema,
        "notification_type": notification.notification_type,
        "target_roles": [get_role(r) for r in notification.target_roles]
    }
