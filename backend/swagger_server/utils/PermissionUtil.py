import datetime

import connexion

from swagger_server.databaseService import Permission as DBPermission
from swagger_server.models.permission import Permission as ModelPermission


def convert_permission_to_dict(permission) -> dict:
    return {
        "id": str(permission.id),
        "created_date_time": str(permission.created_date_time),
        "updated_date_time": str(permission.updated_date_time),
        "domain": permission.domain,
        "action": permission.action
    }


def convert_permission_to_dict_for_update(permission) -> dict:
    new_permission = dict()
    if permission.created_date_time:
        new_permission["created_date_time"] = str(datetime.datetime.now().isoformat())
    if permission.updated_date_time:
        new_permission["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if permission.domain:
        new_permission["domain"] = permission.domain
    if permission.action:
        new_permission["action"] = permission.action
    return new_permission


def get_permission_action_domain(action, domain):
    ##    permissions = Permission.objects.filter(action=action)
    ##    for pm in permissions:
    ##        if pm.domain == domain:
    ##            return pm
    ##    return None
    pass
