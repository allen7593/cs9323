from swagger_server.databaseService import User
from swagger_server.utils import get_current_user


def validate_permission(domain: str, action: str) -> bool:
    current_user: User = get_current_user()
    current_roles = current_user.roles
    for role in current_roles:
        current_permissions = role.permissions
        for permission in current_permissions:
            if permission.domain == "*" and permission.action == "*":
                return True
            elif permission.domain == domain and permission.action == action:
                return True
    return False
