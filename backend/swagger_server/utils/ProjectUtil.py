import datetime

from swagger_server.databaseService import Term
from swagger_server.models.term import Term as ModelTerm
from swagger_server.utils import convert_term_to_dict, get_current_user


def convert_project_to_dict(project):
    return {
        "id": str(project.id),
        "name": project.name,
        "term": convert_term_to_dict(project.term),
        "description": project.description,
        "created_date_time": str(project.created_date_time),
        "updated_date_time": str(project.updated_date_time),
        "max_number": project.max_number
    }


def convert_project_to_dict_for_create(project):
    new_project = dict()
    new_project["name"] = project.name
    new_project["description"] = project.description
    new_project["created_date_time"] = str(datetime.datetime.now().isoformat())
    new_project["updated_date_time"] = str(datetime.datetime.now().isoformat())
    new_project["max_number"] = project.max_number
    new_project["term"] = get_current_user().term
    return new_project


def convert_project_to_dict_for_update(project):
    exit_project = dict()
    if project.id:
        exit_project["id"] = project.id
    else:
        return None
    exit_project["name"] = project.name
    if project.description:
        exit_project["description"] = project.description
    if project.updated_date_time:
        exit_project["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if project.max_number:
        exit_project["max_number"] = project.max_number
    return exit_project
