import datetime

from swagger_server.databaseService import Role, Permission
from swagger_server.databaseService.tables.Role import Role as DBRole
from swagger_server.models import Role as ModelRole
from swagger_server.models import Permission as ModelPermission
from swagger_server.utils.PermissionUtil import convert_permission_to_dict


def convert_role_to_dict(role):
    return {
        "id": str(role.id),
        "created_date_time": str(role.created_date_time),
        "updated_date_time": str(role.updated_date_time),
        "role_name": role.role_name,
        "description": role.description,
        "permissions": [convert_permission_to_dict(p) for p in role.permissions]
    }


def get_role(role):
    return Role.objects.get(role_name=role.role_name)


def convert_role_to_dict_for_create(role):
    new_role = dict()
    new_role["role_name"] = role.role_name
    new_role["description"] = role.description
    new_role["created_date_time"] = str(datetime.datetime.now().isoformat())
    new_role["updated_date_time"] = str(datetime.datetime.now().isoformat())

    new_role["permissions"] = []
    for p in role.permissions:
        permission = None
        if isinstance(p, str):
            permission = Permission.objects.get(id=p)
        elif isinstance(p, dict):
            permissions = Permission.objects.filter(action=p["action"])
            for pm in permissions:
                if str(pm.domain) is p["domain"]:
                    permission = pm
        elif isinstance(p, ModelPermission):
            permissions = Permission.objects.filter(action=p.actions)
            for pm in permissions:
                if pm.domain == p.domain:
                    permission = pm
        new_role["permissions"].append(permission)

    return new_role


def convert_role_to_dict_for_update(role):
    exit_role = dict()
    if role.role_name:
        exit_role["role_name"] = role.role_name
    else:
        return None
    if role.description:
        exit_role["description"] = role.description
    if role.updated_date_time:
        exit_role["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if role.permissions:
        exit_role["permissions"] = []
        for p in role.permissions:
            permission = None
            if isinstance(p, str):
                permission = Permission.objects.get(id=p)
            elif isinstance(p, dict):
                permissions = Permission.objects.filter(action=p["action"])
                for pm in permissions:
                    if str(pm.domain) is p["domain"]:
                        permission = pm
            elif isinstance(p, ModelPermission):
                print(p.actions)
                permissions = Permission.objects.filter(action=p.actions)
                for pm in permissions:
                    # print(pm.domain)
                    # print(p.domain)
                    if pm.domain == p.domain:
                        permission = pm
                        # print(permission.domain)
            exit_role["permissions"].append(permission)
    return exit_role
