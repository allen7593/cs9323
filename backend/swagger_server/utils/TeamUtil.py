import datetime

from swagger_server.databaseService import Term, User, Project
from swagger_server.databaseService.tables.VoteAndTicket import Vote
from swagger_server.models.term import Term as ModelTerm
from swagger_server.models.user import User as ModelUser
from swagger_server.models.project import Project as ModelProject
from swagger_server.databaseService.tables.Activity import Team as DBTeam
from swagger_server.databaseService.tables.User import User as DBUser
from swagger_server.utils.TermUtil import convert_term_to_dict
from swagger_server.utils.ProjectUtil import convert_project_to_dict
from swagger_server.utils.UserUtil import convert_db_user_to_dict
from swagger_server.utils.UserUtil import get_current_user
from swagger_server.utils.VoteAndTicketUtil import convert_vote_to_dict


def convert_team_to_dict_for_create(team) -> dict:
    new_vote = Vote()
    new_vote.save()
    user: DBUser = get_current_user()
    user.is_team_leader = True
    user.save()
    current_term = user.term
    new_team = dict()
    new_team["name"] = team.name
    new_team["description"] = team.description
    new_team["created_date_time"] = str(datetime.datetime.now().isoformat())
    new_team["updated_date_time"] = str(datetime.datetime.now().isoformat())
    new_team["vote"] = Vote.objects.get(id=new_vote.id)
    #new_team["team_members"] = [User.objects.get(id=m.id) for m in team.team_members]

    new_team["projects"] = []
    for p in team.projects:
        if isinstance(p, str):
            project = Project.objects.get(id=p)
        elif isinstance(p, dict):
            project = Project.objects.get(name=p["name"])
        elif isinstance(p, ModelProject):
            project = Project.objects.get(id=p.id)
        else:
            project = None
        new_team["projects"].append(project)

    new_team["team_members"] = []
    new_team["team_members"].append(user)

    if isinstance(team.assignee, str):
        assignee = User.objects.get(id=team.assignee)
    elif isinstance(team.assignee, dict):
        assignee = User.objects.get(email_address=team.assignee["email_address"])
    elif isinstance(team.assignee, ModelUser):
        assignee = User.objects.get(id=team.assignee.id)
    else:
        assignee = None
    new_team["assignee"] = assignee

    if isinstance(team.term, str):
        term = Term.objects.get(id=team.term)
    elif isinstance(team.term, dict):
        term = Term.objects.get(term_name=team.term["term_name"])
    elif isinstance(team.term, ModelTerm):
        term = Term.objects.get(id=team.term.id)
    else:
        term = current_term
    new_team["term"] = term
    return new_team


def convert_db_team_to_dict(team) -> dict:
    if team.assignee:
        assignee = convert_db_user_to_dict(team.assignee)
    else:
        assignee = {}

    return {
        "id": str(team.id),
        "name": team.name,
        "term": convert_term_to_dict(team.term),
        "created_date_time": str(team.created_date_time),
        "updated_date_time": str(team.updated_date_time),
        "description": team.description,
        "projects": [convert_project_to_dict(p) for p in team.projects],
        "team_members": [convert_db_user_to_dict(m) for m in team.team_members],
        "assignee": assignee,
        "vote": convert_vote_to_dict(team.vote)
    }


def convert_team_to_dict_for_update(team):
    exit_team = dict()
    if team.id:
        exit_team["id"] = team.id
    else:
        return None
    if team.name:
        exit_team["name"] = team.name
    if team.description:
        exit_team["description"] = team.description
    exit_team["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if team.term:
        exit_team["term"] = Term.objects.get(id=team.term.id)
    if team.assignee:
        if team.assignee.id:
            exit_team["assignee"] = User.objects.get(id=team.assignee.id)
        else:
            exit_team["assignee"] = None
    else:
        exit_team["assignee"] = None
    if team.projects:
        exit_team["projects"] = []
        for p in team.projects:
            project = Project.objects.get(id=p.id)
            exit_team["projects"].append(project)
    if team.team_members:
        exit_team["team_members"] = []
        for m in team.team_members:
            member = User.objects.get(id=m.id)
            exit_team["team_members"].append(member)
    return exit_team


def get_current_team():
    user: DBUser = get_current_user()
    dict_user = convert_db_user_to_dict(user)
    all_db_team = DBTeam.objects()
    for t in all_db_team:
        for m in t.team_members:
            if m.email_address == dict_user['email_address']:
                return t
    return None
