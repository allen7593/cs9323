import datetime

import connexion

from swagger_server.databaseService import User as DBUser
from swagger_server.databaseService import Term as DBTerm
from swagger_server.models.term import Term as ModelTerm
from swagger_server.utils import UserUtil


def convert_term_to_dict(term):
    return {
        "id": str(term.id),
        "created_date_time": str(term.created_date_time),
        "updated_date_time": str(term.updated_date_time),
        "term_name": term.term_name,
        "description": term.description,
        "start_date": str(term.start_date),
        "end_date": str(term.end_date)
    }


def convert_term_to_dict_for_update(term) -> dict:
    new_term = dict()
    if term.id:
        new_term["id"] = term.id
    else:
        return None
    if term.updated_date_time:
        new_term["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if term.term_name:
        new_term["term_name"] = term.term_name
    if term.description:
        new_term["description"] = str(term.description)
    if term.start_date:
        new_term["start_date"] = term.start_date
    if term.end_date:
        new_term["end_date"] = term.end_date
    return new_term


def show_current_term() -> dict:
    current_term = UserUtil.get_current_user().term
    return current_term
