import datetime

import connexion

from swagger_server.databaseService import User as DBUser
from swagger_server.jwtService import JwtService
from swagger_server.models.user import User as ModelUser
from swagger_server.databaseService.tables.Session import Session
from swagger_server.utils.RoleUtil import convert_role_to_dict
from swagger_server.utils.TermUtil import convert_term_to_dict


def get_current_user() -> DBUser:
    auth_header = connexion.request.headers['Authorization']
    auth_token = auth_header.replace("Bearer ", "")
    token = JwtService.decode_token(auth_token)
    users = DBUser.objects(email_address=token["sub"])
    if users.count() > 0:
        return users[0]
    return None


def convert_db_user_to_dict(user) -> dict:
    user_dict = {
        "first_name": user.first_name,
        "middle_name": user.middle_name,
        "last_name": user.last_name,
        "dob": str(user.dob),
        "email_address": user.email_address,
        "gender": user.gender,
        "created_date_time": str(user.created_date_time),
        "updated_date_time": str(user.updated_date_time),
        "description": user.description,
        "is_team_leader": user.is_team_leader,
        "roles": [convert_role_to_dict(r) for r in user.roles],
        "term": convert_term_to_dict(user.term)
    }
    if user.id:
        user_dict["id"] = str(user.id)
    return user_dict


def convert_user_to_dict_for_update(user) -> dict:
    new_user = dict()
    if user.first_name:
        new_user["first_name"] = user.first_name
    if user.middle_name:
        new_user["middle_name"] = user.middle_name
    if user.last_name:
        new_user["last_name"] = user.last_name
    if user.dob:
        new_user["dob"] = str(user.dob)
    if user.email_address:
        new_user["email_address"] = user.email_address
    if user.gender:
        new_user["gender"] = user.gender
    if user.updated_date_time:
        new_user["updated_date_time"] = str(datetime.datetime.now().isoformat())
    if user.description:
        new_user["description"] = user.description
    if user.is_team_leader:
        new_user["is_team_leader"] = user.is_team_leader
    # if user.roles:
    #     new_user["role"] = [convert_role_to_dict(r) for r in user.roles]
    # if user.term:
    #     new_user["term"] = convert_term_to_dict(user.term)
    return new_user


def get_user(user) -> DBUser:
    return DBUser.objects.get(email_address=user.email_address)
