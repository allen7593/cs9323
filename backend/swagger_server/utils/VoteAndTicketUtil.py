import datetime

from swagger_server.databaseService import Project
from swagger_server.databaseService.tables.Ticket import Ticket
from swagger_server.databaseService.tables.VoteAndTicket import Vote as DBVote
from swagger_server.models import Ticket as ModelTicket
from swagger_server.utils.UserUtil import get_current_user
from swagger_server.utils.ProjectUtil import convert_project_to_dict
from swagger_server.utils.UserUtil import convert_db_user_to_dict


def model_ticket_to_db_ticket(ticket: ModelTicket):
    return Ticket(**{
        "created_date_time": str(datetime.datetime.now().isoformat()),
        "updated_date_time": str(datetime.datetime.now().isoformat()),
        "user": get_current_user(),
        "project": Project.objects.get(id=ticket.project_id)
    })


def convert_ticket_to_dict(ticket: Ticket):
    return {
        "id": ticket.id,
        "user": convert_db_user_to_dict(ticket.user),
        "project_id": ticket.project.id
    }


def convert_vote_to_dict(vote: DBVote):
    new_vote = {
        "id": str(vote.id)
    }
    if vote.voted_project:
        new_vote["voted_project"] = convert_project_to_dict(vote.voted_project)
    if vote.tickets:
        new_vote["tickets"] = [convert_ticket_to_dict(t) for t in vote.tickets]
    return new_vote
