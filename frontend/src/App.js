import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import MomentUtils from "@date-io/moment";
import { Provider } from "react-redux";
import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
// import { renderRoutes } from "react-router-config";
import Routes from "./routes";
import theme from "./theme";
import "./utils/axiosSettings.js";
// import "./assets/app.scss";

// const store = configureStore();
const App = () => {
  return (
    // <Provider store={store}>
    <ThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <Router>
          <Routes />
        </Router>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
    // </Provider>
  );
};

export default App;
