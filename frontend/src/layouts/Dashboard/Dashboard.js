import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/styles";
import { LinearProgress } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { SideBar, TopBar, ChatWidget } from "./components";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    overflow: "hidden",
    backgroundColor: "#f0f2f5"
  },
  topBar: {
    zIndex: 2
  },
  container: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden"
  },
  sideBar: {
    zIndex: 3,
    width: 256,
    minWidth: 256,
    flex: "0 0 auto"
  },
  pageContainer: {
    minHeight: "calc(100vh - 96px)",
    [theme.breakpoints.up("md")]: {
      paddingLeft: 240
    },
    marginTop: 90
  },
  content: {
    flex: "1 1 auto"
  }
}));

const Dashboard = props => {
  const { children } = props;
  const classes = useStyles();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up("md"), {
    defaultMatches: true
  });

  const [openSidebar, setOpenSidebar] = useState(false);

  const handleSidebarOpen = () => {
    setOpenSidebar(true);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  const shouldOpenSidebar = isDesktop ? true : openSidebar;

  return (
    <div className={classes.root}>
      <TopBar className={classes.topBar} onSidebarOpen={handleSidebarOpen} />

      <main className={classes.content}>
        <SideBar
          className={classes.sideBar}
          onClose={handleSidebarClose}
          open={shouldOpenSidebar}
          variant={isDesktop ? "persistent" : "temporary"}
          PaperProps={"style = {{ backgroundColor: 'black' }}"}
        />
        <div className={classes.pageContainer}>{children}</div>
      </main>
      <ChatWidget />
    </div>
  );
};

Dashboard.propTypes = {
  route: PropTypes.object
};

export default Dashboard;
