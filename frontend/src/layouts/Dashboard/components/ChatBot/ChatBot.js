import React, { Fragment, useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import {
  Drawer,
  Fab,
  Card,
  CardContent,
  CardActions,
  Button,
  Typography,
  ClickAwayListener,
  Paper
} from "@material-ui/core";
import ForumIcon from "@material-ui/icons/Forum";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  fab: {
    position: "absolute",
    bottom: theme.spacing(3),
    right: theme.spacing(3)
  },
  chatBox: {
    position: "absolute",
    bottom: theme.spacing(12),
    right: theme.spacing(3),
    boxShadow: "0 6px 100px 0 rgba(0,0,0,.35)"
  }
}));

const ChatBot = props => {
  const classes = useStyles();
  const [boxOpen, setBoxOpen] = useState(false);

  //   useEffect(() => {
  //     let mounted = true;

  //     const fetchData = () => {
  //       axios.get("/api/chat").then(response => {
  //         if (mounted) {
  //           setData(response.data);
  //         }
  //       });
  //     };

  //     fetchData();

  //     return () => {
  //       mounted = false;
  //     };
  //   }, []);

  const handleFabClick = () => {
    !boxOpen && setBoxOpen(true);
    boxOpen && setBoxOpen(false);
  };

  const handleBoxClose = () => {
    setBoxOpen(false);
    console.log("awau");
  };
  // if (!data) {
  //   return null;
  // }

  return (
    // <ClickAwayListener>
    <div>
      <Fab
        aria-label="Add"
        className={classes.fab}
        color="primary"
        onClick={handleFabClick}
      >
        {boxOpen ? <CloseIcon /> : <ForumIcon />}
      </Fab>

      {boxOpen ? (
        <Card className={classes.chatBox}>
          <CardContent>
            <Typography variant="h5" component="h2">
              This is content
            </Typography>
          </CardContent>
          <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions>
        </Card>
      ) : null}
    </div>
    // </ClickAwayListener>
  );
};

export default ChatBot;
