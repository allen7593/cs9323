import React, { Component } from "react";
import { Widget, addResponseMessage, addUserMessage } from "react-chat-widget";
import axios from "axios";
import "react-chat-widget/lib/styles.css";

const url = "http://localhost:8080/qanda";

class ChatWidget extends Component {
  componentDidMount() {
    addResponseMessage("Welcome to this awesome chat!");
  }

  handleNewUserMessage = newMessage => {
    console.log(`New message incoming! ${newMessage}`);
    // Now send the message throught the backend API
    // addResponseMessage("response");

    axios
      .put(`${url}/${newMessage}`)
      .then(res => {
        console.log(res);
        for (var i = 0; i < res.data.length; i++) {
          addResponseMessage(res.data[i]);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="App">
        <Widget
          handleNewUserMessage={this.handleNewUserMessage}
          title="COMP 9323"
          subtitle="Intelligent Chatbot"
        />
      </div>
    );
  }
}

export default ChatWidget;
