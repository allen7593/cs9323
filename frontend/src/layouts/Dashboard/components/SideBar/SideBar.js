import React, { forwardRef } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import { NavLink as RouterLink, withRouter } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Button from "@material-ui/core/Button";
import blueGrey from "@material-ui/core/colors/blueGrey";
import DashboardIcon from "@material-ui/icons/Dashboard";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import AccountTreeIcon from "@material-ui/icons/AccountTree";
import EventNoteIcon from "@material-ui/icons/EventNote";
import GroupIcon from "@material-ui/icons/Group";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import { decode } from "jsonwebtoken";
const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up("md")]: {
      marginTop: 64,
      height: "calc(100% - 64px)"
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: "flex",
    flexDirection: "column",
    height: "100%",
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  },
  item: {
    display: "flex",
    paddingTop: 0,
    paddingBottom: 0
  },
  button: {
    color: blueGrey[800],
    padding: "10px 8px",
    justifyContent: "flex-start",
    textTransform: "none",
    letterSpacing: 0,
    width: "100%",
    fontSize: "2vh",
    fontWeight: theme.typography.fontWeightMedium
  },
  icon: {
    color: theme.palette.icon,
    width: 24,
    height: 24,
    display: "flex",
    alignItems: "center",
    marginRight: theme.spacing(1)
  },
  active: {
    borderRadius: 0,
    borderRight: "3px solid " + theme.palette.primary.main,
    backgroundColor: "#eee",
    color: theme.palette.black,
    fontWeight: theme.typography.fontWeightBold,
    "& $icon": {
      color: theme.palette.primary.main
    }
  }
}));

const CustomRouterLink = forwardRef((props, ref) => (
  <div ref={ref} style={{ flexGrow: 1 }}>
    <RouterLink {...props} />
  </div>
));

const SideBar = props => {
  const { open, variant, onClose, className, ...rest } = props;
  const classes = useStyles();
  const token = decode(localStorage.getItem("x-access-token"));
  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <div {...rest}>
        <List {...rest}>
          <ListItem className={classes.item} disableGutters key="Dashboard">
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={CustomRouterLink}
              to="/dashboard"
              onClick={onClose}
            >
              <div className={classes.icon}>
                <DashboardIcon />
              </div>
              Dashboard
            </Button>
          </ListItem>
          <ListItem className={classes.item} disableGutters key="Team">
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={CustomRouterLink}
              to="/team"
              onClick={onClose}
            >
              <div className={classes.icon}>
                <GroupIcon />
              </div>
              Team
            </Button>
          </ListItem>
          <ListItem className={classes.item} disableGutters key="Project">
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={CustomRouterLink}
              to="/project"
              onClick={onClose}
            >
              <div className={classes.icon}>
                <AccountTreeIcon />
              </div>
              Project
            </Button>
          </ListItem>
          {token.roles[0]['role_name'] == "student" ? null :
            <ListItem className={classes.item} disableGutters key="Notification">
              <Button
                activeClassName={classes.active}
                className={classes.button}
                component={CustomRouterLink}
                to="/notification"
                onClick={onClose}
              >
                <div className={classes.icon}>
                  <EventNoteIcon />
                </div>
                Notification
            </Button>
            </ListItem>
          }
          <ListItem className={classes.item} disableGutters key="Term">
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={CustomRouterLink}
              to="/term"
              onClick={onClose}
            >
              <div className={classes.icon}>
                <LibraryBooksIcon />
              </div>
              Term
            </Button>
          </ListItem>
        </List>
      </div>
    </Drawer>
  );
};

export default withRouter(SideBar);
