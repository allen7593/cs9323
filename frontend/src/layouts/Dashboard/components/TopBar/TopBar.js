/* eslint-disable no-unused-vars */
import React, { useState, useRef, useEffect } from "react";
import { Link as RouterLink, withRouter } from "react-router-dom";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import {
  AppBar,
  Badge,
  Button,
  IconButton,
  Toolbar,
  Hidden,
  Typography,
  Menu,
  MenuItem
} from "@material-ui/core";
import AccountCircle from "@material-ui/icons/AccountCircle";
import InputIcon from "@material-ui/icons/Input";
import MenuIcon from "@material-ui/icons/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import FaceIcon from "@material-ui/icons/Face";
import SearchIcon from "@material-ui/icons/Search";

import axios from "axios";
// import useRouter from "../../../../utils/useRouter";

const useStyles = makeStyles(theme => ({
  root: {},
  flexGrow: {
    flexGrow: 1
  },
  search: {
    backgroundColor: "rgba(255,255,255, 0.1)",
    borderRadius: 4,
    flexBasis: 300,
    height: 36,
    padding: theme.spacing(0, 2),
    display: "flex",
    alignItems: "center"
  },
  searchIcon: {
    marginRight: theme.spacing(2),
    color: "inherit"
  },
  searchInput: {
    flexGrow: 1,
    color: "inherit",
    "& input::placeholder": {
      opacity: 1,
      color: "inherit"
    }
  },
  searchPopper: {
    zIndex: theme.zIndex.appBar + 100
  },
  searchPopperContent: {
    marginTop: theme.spacing(1)
  },
  icon: {
    marginRight: theme.spacing(1)
  }
}));

const TopBar = props => {
  const { onSidebarOpen, className, ...rest } = props;
  const classes = useStyles();
  // console.log(props);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = text => {
    setAnchorEl(null);
    console.log(text);
    props.history.push(`/${text}`);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.clear();
    props.history.push("/login");
  };

  return (
    <AppBar {...rest} position="fixed" style={{ backgroundColor: "#f27b53" }}>
      <Toolbar>
        <RouterLink to="/" style={{ textDecoration: "none" }}>
          <Typography variant="h4" style={{ color: "#fff" }}>
            <b>COMP 9323</b>
          </Typography>
        </RouterLink>
        <div className={classes.flexGrow} />
        <div>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            keepMounted
            open={open}
            onClose={handleMenuClose}
          >
            <MenuItem onClick={() => handleClose("profile")}>
              <FaceIcon className={classes.icon} />
              Profile
            </MenuItem>
            <MenuItem onClick={handleLogout}>
              <InputIcon className={classes.icon} />
              Sign out
            </MenuItem>
          </Menu>
        </div>
        <Hidden mdUp>
          <IconButton color="inherit" onClick={onSidebarOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

export default withRouter(TopBar);
