import React, { useState, useEffect, Fragment } from "react";
import { useHistory } from "react-router-dom";
import {
  Grid,
  Paper,
  Card,
  Typography,
  Container,
  CardHeader,
  CardContent,
  Button,
  Divider
} from "@material-ui/core";
import EventNoteIcon from "@material-ui/icons/EventNote";
import AccessAlarmIcon from "@material-ui/icons/AccessAlarm";
import FaceIcon from "@material-ui/icons/Face";
import Notices from "./components/Notices";
import { makeStyles } from "@material-ui/styles";
import axios from "axios";
import { decode } from "jsonwebtoken";
import NoticeForm from "./components/NoticeForm";

const useStyles = makeStyles(theme => ({
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  dashboardIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  noticeIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #4e9bcf, #1976d2)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(25, 118, 210,.4)"
  },
  dueIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #f27b53, #fb8c00)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(255, 152, 0,.4)"
  }
}));
const Dashboard = props => {
  const classes = useStyles();
  const [data, setData] = useState({
    firstName: "",
    description: ""
  });
  const history = useHistory();
  const token = decode(localStorage.getItem("x-access-token"));

  useEffect(() => {
    console.log("this is token");
    console.log(token);
    const { user_id } = token;

    axios
      .get(`/user/${user_id}`)
      .then(res => {
        console.log(res);
        setData({
          firstName: res.data.first_name,
          description: res.data.description
        });
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <Container maxWidth="lg">
      <Grid container justify="space-around" alignItems="flex-start">
        <Grid item md={8}>
          <Card className={classes.grid}>
            <div className={classes.dashboardIcon}>
              <FaceIcon />
            </div>
            <CardHeader
              title={
                <Fragment>
                  <Typography variant="h3" gutterBottom>
                    <b>{`Good Day, ${data.firstName}`}</b>
                  </Typography>
                  <Typography variant="h5" gutterBottom>
                    {data.description}
                  </Typography>
                </Fragment>
              }
            />
          </Card>
          <Card className={classes.grid} style={{ marginTop: 40 }}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="stretch"
            >
              <Grid item md={9}>
                <div className={classes.noticeIcon}>
                  <EventNoteIcon />
                </div>
                <CardHeader
                  title={
                    <Typography variant="h4" gutterBottom>
                      Notifications
                    </Typography>
                  }
                />
              </Grid>
              <Grid item md={2} className={classes.grid}>
                {token.roles[0]["role_name"] == "student" ? null : (
                  <NoticeForm />
                )}
              </Grid>
            </Grid>

            <CardContent>
              <Notices />
            </CardContent>
          </Card>
        </Grid>
        <Grid item md={4}>
          <Card className={classes.grid}>
            <div className={classes.dueIcon}>
              <AccessAlarmIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h4" gutterBottom>
                  Recent Due
                </Typography>
              }
            />
            <Divider />
            <CardContent>phase 1 due in 5 days</CardContent>
            <Divider />
            <CardContent>assignment 2 due in 10 days</CardContent>
            <Divider />
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Dashboard;
