import React, { Component, Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Card, CardContent, CardHeader } from "@material-ui/core";
import axios from "axios";
import dayjs from "dayjs";

const Res = [
  {
    colour_schema: "",
    created_date_time: "2019-11-10 00:28:03.312000",
    description: "Term Start",
    instance_notification: false,
    message:
      "Welcome to COMP9323, The new term is start at 2019-08-10 00:00:00, please keep tracking this website for more information",
    name: "Term Start",
    notification_type: "Notification",
    pre_release_days: 5,
    published: true,
    publisher: {
      created_date_time: "2019-05-20 00:00:00",
      description: "Open-architected grid-enabled Graphical User Interface",
      dob: "1994-08-02",
      email_address: "tloveday0@symantec.com",
      first_name: "Blanch",
      gender: "Female",
      is_team_leader: null,
      last_name: "Loveday",
      middle_name: "Tobi",
      roles: [
        {
          created_date_time: "2019-05-20 00:00:00",
          description: "admin",
          permissions: [
            {
              action: "*",
              created_date_time: "2019-05-20 00:00:00",
              domain: "*",
              updated_date_time: "2019-05-20 00:00:00"
            }
          ],
          role_name: "admin",
          updated_date_time: "2019-05-20 00:00:00"
        }
      ],
      term: {
        created_date_time: "2019-05-20 00:00:00",
        description: "Voltsillam",
        end_date: "2019-12-20 00:00:00",
        start_date: "2019-08-10 00:00:00",
        term_name: "2019T3",
        updated_date_time: "2019-05-20 00:00:00"
      },
      updated_date_time: "2019-05-20 00:00:00"
    },
    release_date: "2019-08-10 00:00:00",
    target_roles: [
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "admin",
        permissions: [
          {
            action: "*",
            created_date_time: "2019-05-20 00:00:00",
            domain: "*",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "admin",
        updated_date_time: "2019-05-20 00:00:00"
      },
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "mentor",
        permissions: [
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "delete",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "search",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "assign_user",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_member",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_project",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "*",
            created_date_time: "2019-05-20 00:00:00",
            domain: "notification",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "self_edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "term",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "mentor",
        updated_date_time: "2019-05-20 00:00:00"
      },
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "student",
        permissions: [
          {
            action: "create",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "delete",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "search",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_member",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "notification",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "self_edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "term",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "student",
        updated_date_time: "2019-05-20 00:00:00"
      }
    ],
    term: {
      created_date_time: "2019-05-20 00:00:00",
      description: "Voltsillam",
      end_date: "2019-12-20 00:00:00",
      start_date: "2019-08-10 00:00:00",
      term_name: "2019T3",
      updated_date_time: "2019-05-20 00:00:00"
    },
    updated_date_time: "2019-11-10 00:28:03.312000"
  },
  {
    colour_schema: "",
    created_date_time: "2019-11-10 00:28:03.312000",
    description: "Term Start",
    instance_notification: false,
    message:
      "Welcome to COMP9323, The new term is start at 2019-08-10 00:00:00, please keep tracking this website for more information",
    name: "Term Start",
    notification_type: "Notification",
    pre_release_days: 5,
    published: true,
    publisher: {
      created_date_time: "2019-05-20 00:00:00",
      description: "Open-architected grid-enabled Graphical User Interface",
      dob: "1994-08-02",
      email_address: "tloveday0@symantec.com",
      first_name: "Blanch",
      gender: "Female",
      is_team_leader: null,
      last_name: "Loveday",
      middle_name: "Tobi",
      roles: [
        {
          created_date_time: "2019-05-20 00:00:00",
          description: "admin",
          permissions: [
            {
              action: "*",
              created_date_time: "2019-05-20 00:00:00",
              domain: "*",
              updated_date_time: "2019-05-20 00:00:00"
            }
          ],
          role_name: "admin",
          updated_date_time: "2019-05-20 00:00:00"
        }
      ],
      term: {
        created_date_time: "2019-05-20 00:00:00",
        description: "Voltsillam",
        end_date: "2019-12-20 00:00:00",
        start_date: "2019-08-10 00:00:00",
        term_name: "2019T3",
        updated_date_time: "2019-05-20 00:00:00"
      },
      updated_date_time: "2019-05-20 00:00:00"
    },
    release_date: "2019-08-10 00:00:00",
    target_roles: [
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "admin",
        permissions: [
          {
            action: "*",
            created_date_time: "2019-05-20 00:00:00",
            domain: "*",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "admin",
        updated_date_time: "2019-05-20 00:00:00"
      },
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "mentor",
        permissions: [
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "delete",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "search",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "assign_user",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_member",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_project",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "*",
            created_date_time: "2019-05-20 00:00:00",
            domain: "notification",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "self_edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "term",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "mentor",
        updated_date_time: "2019-05-20 00:00:00"
      },
      {
        created_date_time: "2019-05-20 00:00:00",
        description: "student",
        permissions: [
          {
            action: "create",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "delete",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "search",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "edit_member",
            created_date_time: "2019-05-20 00:00:00",
            domain: "team",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "project",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "notification",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "self_edit",
            created_date_time: "2019-05-20 00:00:00",
            domain: "user",
            updated_date_time: "2019-05-20 00:00:00"
          },
          {
            action: "view",
            created_date_time: "2019-05-20 00:00:00",
            domain: "term",
            updated_date_time: "2019-05-20 00:00:00"
          }
        ],
        role_name: "student",
        updated_date_time: "2019-05-20 00:00:00"
      }
    ],
    term: {
      created_date_time: "2019-05-20 00:00:00",
      description: "Voltsillam",
      end_date: "2019-12-20 00:00:00",
      start_date: "2019-08-10 00:00:00",
      term_name: "2019T3",
      updated_date_time: "2019-05-20 00:00:00"
    },
    updated_date_time: "2019-11-10 00:28:03.312000"
  }
];

const Notices = props => {
  const [data, setData] = useState(Res);
  const [notice, setNotice] = useState([]);
  useEffect(() => {
    axios.get("/notifications").then(res => {
      console.log(res);
      setNotice(res.data);
    });
  }, []);
  return (
    <div>
      {/* {data.map((item, index) => (
      <Card style={{ marginBottom: 15 }} key={`notice-${index}`}>
        <CardHeader
          title={
            <Typography variant="h3" gutterBottom>
              <b>{item.name}</b>
            </Typography>
          }
          subheader={`Posted by ${item.publisher.first_name}, last modified at ${item.updated_date_time}`}
        />
        <CardContent
          dangerouslySetInnerHTML={{ __html: item.message }}
        ></CardContent>
      </Card>
    ))} */}
      {notice.reverse().map(item => (
        <Card style={{ marginBottom: 10 }}>
          <CardHeader
            title={
              <Typography variant="h3" gutterBottom>
                <b>{item.name}</b>
              </Typography>
            }
            subheader={`Posted by ${
              item.publisher.first_name
            }, last modified at ${dayjs(item.updated_date_time0).format(
              "YYYY-MM-DD HH:mm:ss"
            )}`}
          />
          <CardContent dangerouslySetInnerHTML={{ __html: item.message }} />
        </Card>
      ))}
    </div>
  );
};

export default Notices;
