import React, { Fragment } from "react";
import { Link as RouterLink } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardContent,
  CardMedia,
  Typography,
  Toolbar,
  AppBar
} from "@material-ui/core";
import AccountCircle from "@material-ui/icons/AccountCircle";
import LockIcon from "@material-ui/icons/Lock";

// import gradients from "utils/gradients";
import LoginForm from "./components/LoginForm";
const logo = require("../../assets/images/login-icon.png");
const useStyles = makeStyles(theme => ({
  root: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing(6, 2)
  },
  card: {
    left: "25%",
    top: "25%",
    // marginTop: "25vh",
    position: "absolute",
    display: "flex"
  },
  content: {
    padding: theme.spacing(6, 4, 3, 4)
  },
  icon: {
    color: theme.palette.white,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    position: "absolute",
    top: -32,
    left: theme.spacing(3),
    height: 64,
    width: 64,
    fontSize: 32
  },
  loginForm: {
    marginTop: theme.spacing(3),
    maxWidth: 350
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  person: {
    marginTop: theme.spacing(2),
    display: "flex"
  },
  cover: {
    width: 370,
    height: 370,
    margin: theme.spacing(2)
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

const Login = props => {
  const classes = useStyles();
  console.log(props);

  return (
    <Fragment>
      <AppBar position="fixed" style={{ backgroundColor: "#f27b53" }}>
        <Toolbar>
          <Typography variant="h4" style={{ color: "#fff" }}>
            <b>COMP 9323</b>
          </Typography>
        </Toolbar>
      </AppBar>
      <div
        style={{ width: "100vw", height: "100vh", backgroundColor: "#f1f2f3" }}
      >
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            <Typography gutterBottom variant="h3" align="center">
              COMP 9323 Platfrom
            </Typography>
            <Typography gutterBottom variant="h4" align="center">
              Login
            </Typography>
            {/* <Typography variant="subtitle2">
            Sign in on the internal platform
          </Typography> */}
            <LoginForm className={classes.loginForm} />
            {/* <Divider className={classes.divider} />
          <Link
            align="center"
            color="secondary"
            component={RouterLink}
            to="/auth/register"
            underline="always"
            variant="subtitle2"
          >
            Don 't have an account?
          </Link> */}
          </CardContent>
          <CardMedia
            className={classes.cover}
            image={logo}
            title="Live from space album cover"
          />
        </Card>
      </div>
    </Fragment>
  );
};

export default Login;
