/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import validate from "validate.js";
import clsx from "clsx";
import PropTypes from "prop-types";
// import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/styles";
import { Button, TextField, Typography, useTheme } from "@material-ui/core";
import RouteWithLayout from "../../../../components/RouteWithLayout";
import { withRouter } from "react-router-dom";
import axios from "axios";
// import useRouter from "utils/useRouter";
// import { login } from "actions";

const schema = {
  email: {
    presence: {
      allowEmpty: false,
      message: "is required"
    },
    email: true
  },
  password: {
    presence: {
      allowEmpty: false,
      message: "is required"
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  fields: {
    margin: theme.spacing(-1),
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      flexGrow: 1,
      margin: theme.spacing(1)
    }
  },
  submitButton: {
    marginTop: theme.spacing(2),
    width: "100%"
  },
  loginError: {
    color: theme.palette.error.main,
    marginTop: theme.spacing(1),
    textAlign: "center"
  }
}));

const LoginForm = props => {
  const { className, ...rest } = props;
  console.log(props);
  const classes = useStyles();
  const [errorOpen, setErrorOpen] = useState("none");
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();
    setErrorOpen("none");
    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSubmit = () => {
    console.log(formState);
    var data = {
      user_name: formState.values.email,
      password: formState.values.password
    };
    console.log(data);
    axios
      .post("/auth", data)
      .then(async res => {
        console.log(res);
        if (res.status === 200) {
          console.log(res);
          await localStorage.setItem("x-access-token", res.data);
          await props.history.push("/dashboard");
        }
      })
      .catch(err => {
        setErrorOpen("block");
        console.log(err);
      });
    // props.history.push("/");
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <form {...rest} className={clsx(classes.root, className)}>
      <div className={classes.fields}>
        <TextField
          error={hasError("email")}
          fullWidth
          helperText={hasError("email") ? formState.errors.email[0] : null}
          label="email"
          name="email"
          onChange={handleChange}
          value={formState.values.email || ""}
          variant="outlined"
        />
        <TextField
          error={hasError("password")}
          fullWidth
          helperText={
            hasError("password") ? formState.errors.password[0] : null
          }
          label="Password"
          name="password"
          onChange={handleChange}
          type="password"
          value={formState.values.password || ""}
          variant="outlined"
        />
      </div>
      <Button
        className={classes.submitButton}
        color="primary"
        disabled={!formState.isValid}
        size="large"
        variant="contained"
        onClick={handleSubmit}
      >
        Sign in
      </Button>
      <Typography style={{ display: errorOpen }} className={classes.loginError}>
        Invalid Email or Password.
      </Typography>
      {"tloveday0@symantec.com, Te0ldjd"}
      <br />
      {"fgulland8@weather.com, odsXTOp"}
      <br />
      {"cbelleniee@boston.com, ETFwnfmnQ9"}
      <br />
      {"jdymock12@ebay.co.uk, cUxrkJ"}
    </form>
  );
};

LoginForm.propTypes = {
  className: PropTypes.string
};

export default withRouter(LoginForm);
