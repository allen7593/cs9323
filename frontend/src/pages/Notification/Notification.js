import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  Container,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Typography,
  Grid
} from "@material-ui/core";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import NoticeForm from "./components/NoticeForm";
import axios from "axios";
import dayjs from "dayjs";

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(3)
  },
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  noteIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  }
}));

const Notification = () => {
  const classes = useStyles();
  const [notice, setNotice] = useState([]);
  useEffect(() => {
    axios
      .get("/notifications")
      .then(res => {
        console.log(res);
        setNotice(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <Container maxWidth="lg">
      <Grid container direction="row">
        <Grid item md={12} sm={12}>
          <Card className={classes.grid}>
            <div className={classes.noteIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Notification Management</b>
                </Typography>
              }
            />
            <CardContent>
              <NoticeForm />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <Grid container direction="row">
        <Grid item md={12} sm={12} style={{ marginTop: 40 }}>
          <Card className={classes.grid}>
            <div className={classes.noteIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Notifications</b>
                </Typography>
              }
            />
            {notice.reverse().map(item => (
              <Card style={{ marginBottom: 10 }}>
                <CardHeader
                  title={
                    <Typography variant="h3" gutterBottom>
                      <b>{item.name}</b>
                    </Typography>
                  }
                  subheader={`Posted by ${
                    item.publisher.first_name
                  }, last modified at ${dayjs(item.updated_date_time0).format(
                    "YYYY-MM-DD HH:mm:ss"
                  )}`}
                />
                <CardContent
                  dangerouslySetInnerHTML={{ __html: item.message }}
                />
              </Card>
            ))}
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Notification;
