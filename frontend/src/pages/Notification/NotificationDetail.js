import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  Container,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Typography
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(3)
  }
}));

const NotificationDetail = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="lg">
      <Card>
        <CardHeader
          variant="h5"
          component="h3"
          title={"This is Notification Content"}
        ></CardHeader>

        <CardContent>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Create Notification
          </Button>
        </CardContent>
      </Card>
    </Container>
  );
};

export default NotificationDetail;
