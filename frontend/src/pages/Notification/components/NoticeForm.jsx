import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import MenuItem from '@material-ui/core/MenuItem';
import { Select, InputLabel,Input,Chip } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import "date-fns";
import DateFnsUtils from '@date-io/date-fns';

import axios from 'axios';

function NoticeForm() {
    const [open, setOpen] = React.useState(false);
    const [immediate, setImmediate] = React.useState(true);
    const [type, setType] = React.useState("Assignment Due");
    const [roleName, setRoleName] = React.useState(["admin"]);
    const [selectedDate, setSelectedDate] = React.useState(new Date('2019-11-13T21:11:54'));
    const [message, setMessage] = React.useState("");
    const [days, setDays] = React.useState(0);
    const [name, setName] = React.useState("test");
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        console.log(roleName);
        setOpen(false);
    };
    const handleSubmit = () => {
        const targetRoles = [];
        for (let i = 0; i < roleName.length; i++){
            targetRoles.push({ "role_name":roleName[i]});
        }
        axios.post("/notifications",
            {
                "created_date_time": "2000-01-23T04:56:07Z",
                "updated_date_time": "2000-01-23T04:56:07Z",
                "name":name,
                "release_date": selectedDate.toISOString(),
                "pre_release_days": days,
                "message": message,
                "instance_notification": immediate,
                "notification_type": type,
                "target_roles": targetRoles,
                "term": {"term_name": "2019T3"}
        }).then(res => { 
            console.log(res);
        }).catch(error => {
            console.log(error);
        });
        setOpen(false);
    };
    const handleDays = event => {
        setDays(parseInt(event.target.value));
    };
    const handleTypeChange = () => {
        setImmediate(!immediate);
    };
    const handleSelect = event => {
        setType(event.target.value);
    };
    const handleMessage = (event) => {
      setMessage(event.target.value)  
    };
    const handleRole = event => {
        setRoleName(event.target.value);
    }
    const handleDateChange = date => {
        setSelectedDate(date);
    };
    const handleName = event => {
        setName(event.target.value);  
    };
    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                Create Notifications
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Notification</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        There are 2 types of notifications: immediate notification and normal notification. 
                        switch on immediate notification, then the notification will be sent immediately. 
                        Otherwise, the notification will be sent scheduled.
                    </DialogContentText>
                    <form>
                        <div>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="title"
                                label="Title"
                                fullWidth
                                onChange={handleName}
                            />
                        </div>
                        <FormControlLabel
                            // className={classes.formControlLabel}
                            control={
                                <Switch
                                    checked={immediate}
                                    onChange={handleTypeChange}
                                    value="fullWidth"
                                />
                            }
                            label="Immediate Notification"
                        />
                        {immediate ? null :
                            <div>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Grid container>
                                        <KeyboardDatePicker
                                        margin="normal"
                                        id="date-picker-dialog"
                                        label="Select Due Date"
                                        format="MM/dd/yyyy"
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                        }}
                                    />
                                    </Grid>
                                </MuiPickersUtilsProvider>
                                <div>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        id="days"
                                        label="Remind Days"
                                        fullWidth
                                        onChange={handleDays}
                                    />
                                </div>
                            </div>
                        }
                        <div>
                        <TextField
                            id="standard-select"
                            select
                            label="Select Type"
                            // className={classes.textField}
                            value={type}
                            onChange={handleSelect}
                            // SelectProps={{
                            //     MenuProps: {
                            //         className: classes.menu,
                            //     },
                            // }}
                            // helperText="Please select type"
                                margin="normal"
                                fullWidth
                        >
                            <MenuItem
                                key="Assignment Due"
                                value="Assignment Due"
                            >
                                Assignment Due
                            </MenuItem>
                            <MenuItem
                            key="Notification"
                            value="Notification"
                            >
                                Notification
                            </MenuItem>
                            <MenuItem
                            key="Remainder"
                            value="Remainder"
                            >
                                Remainder
                            </MenuItem>
                        </TextField>
                        </div>
                        <div className="">
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Message"
                                onChange={handleMessage}
                                fullWidth
                            />
                        </div>
                        <div>
                            <FormControl className="">
                                <InputLabel id="demo-mutiple-chip-label">Role Group</InputLabel>
                                <Select
                                    labelId="demo-mutiple-chip-label"
                                    id="demo-mutiple-chip"
                                    multiple
                                    value={roleName}
                                    onChange={handleRole}
                                    input={<Input id="select-multiple-chip" />}
                                    renderValue={selected => {
                                        console.log(selected);
                                        if (selected.length === 0) {
                                            return <em>Placeholder</em>;
                                        }

                                        return selected.join(', ');
                                    }}
                                    // MenuProps={MenuProps}
                                >
                                <MenuItem
                                    key="admin"
                                    value="admin"
                                >
                                    admin
                                </MenuItem>
                                <MenuItem
                                    key="mentor"
                                    value="mentor"
                                >
                                    mentor
                                </MenuItem>
                                <MenuItem
                                    key="student"
                                    value="student"
                                >
                                    student
                                </MenuItem>
                                </Select>
                            </FormControl>
                        </div>

                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default NoticeForm;