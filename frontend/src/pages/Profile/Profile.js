import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Avatar,
  CardActions,
  Button,
  Typography,
  Divider
} from "@material-ui/core";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import ProfileForm from "./components/ProfileForm";
import Alert from "./components/Alert";
import axios from "axios";
import { isFlowPredicate } from "@babel/types";
import { decode } from "jsonwebtoken";

const useStyles = makeStyles(theme => ({
  root: {},
  card: {
    margin: theme.spacing(1),
    overflow: "visible",
    marginTop: 30
  },
  textMargin: {
    margin: 15
  },
  buttonProfile: {
    marginTop: 40,
    marginBottom: 20
  },
  avatarIcon: {
    padding: 30,
    marginTop: -65,
    marginBottom: 10,
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  profileIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  }
}));
const url = "http://localhost:8080/user";
const Profile = props => {
  const classes = useStyles();
  const url_uid = props.match.params.user_id;
  const { user_id } = decode(localStorage.getItem("x-access-token"));
  const [profileData, setProfileData] = useState({});
  const [formData, setFormData] = useState({
    email_address: "",
    first_name: "",
    middle_name: "",
    last_name: "",
    description: "",
    role: ""
  });
  const [isOther, setIsOther] = useState(true);

  const [loading, setLoading] = useState(true);
  useEffect(() => {
    // const uid = localStorage.getItem("uid");
    console.log("this is", user_id);
    console.log(url_uid);
    if (url_uid === undefined) {
      axios
        .get(`${url}/${user_id}`)
        .then(res => {
          const { data } = res;
          setProfileData(data);
          setFormData({
            email_address: data["email_address"],
            first_name: data["first_name"],
            middle_name: data["middle_name"],
            last_name: data["last_name"],
            description: data["description"],
            role: data["roles"][0]["role_name"]
          });
          setIsOther(false);
          console.log(res);
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      axios
        .get(`${url}/${url_uid}`)
        .then(res => {
          const { data } = res;
          setProfileData(data);
          setFormData({
            email_address: data["email_address"],
            first_name: data["first_name"],
            middle_name: data["middle_name"],
            last_name: data["last_name"],
            description: data["description"]
          });
          setIsOther(true);
          console.log(res);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }, []);

  const handleFormChange = event => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });
    console.log(formData);
  };

  const handleProfileUpdate = data => {
    console.log(profileData);
    console.log(formData);
    axios
      .put(`${url}`, { ...formData, id: user_id })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };
  return (
    <Container maxWidth="lg">
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="flex-start"
      >
        <Grid item md={4}>
          <Card className={classes.card}>
            <CardContent>
              <Grid
                container
                direction="column"
                alignItems="center"
                justify="flex-start"
              >
                <Avatar className={classes.avatarIcon}>
                  <Typography variant="h1" style={{ color: "white" }}>
                    {`${formData["first_name"].slice(0, 1)}${formData[
                      "last_name"
                    ].slice(0, 1)}`}
                  </Typography>
                </Avatar>

                <Typography variant="subtitle2">{formData["role"]}</Typography>
                <Typography className={classes.textMargin} variant="h4">
                  {`${formData["first_name"]} ${formData["middle_name"]} ${formData["last_name"]}`}
                </Typography>
                <Typography variant="body1">
                  {formData["description"]}
                </Typography>
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.buttonProfile}
                >
                  <a
                    href={`mailto:${formData.email_address}`}
                    style={{ textDecoration: "none", color: "white" }}
                  >
                    Mail Me
                  </a>
                </Button>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item md={8}>
          <Card className={classes.card}>
            <div className={classes.profileIcon}>
              <AccountBoxIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Profile</b>
                </Typography>
              }
            />
            <CardContent>
              <ProfileForm
                data={formData}
                isOther={isOther}
                handleFormChange={handleFormChange}
              />
            </CardContent>
            {isOther ? null : (
              <Fragment>
                <Divider />
                <CardActions>
                  {/* <Button
                variant="contained"
                color="secondary"
                onClick={handleProfileUpdate}
              >
                Update
              </Button> */}
                  <Alert onClick={handleProfileUpdate} />
                </CardActions>
              </Fragment>
            )}
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Profile;
