import React, { useState, useEffect } from "react";
import {
  Container,
  Typography,
  TextField,
  Button,
  Divider,
  Card,
  CardActions
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  form: {
    padding: theme.spacing(2)
  }
}));

const ProfileForm = props => {
  const { data, isOther, handleFormChange } = props;
  const classes = useStyles();

  return (
    <form className={classes.form}>
      <TextField
        fullWidth
        InputProps={{
          readOnly: isOther
        }}
        label="Email"
        name="first_name"
        value={data.email_address}
        margin="normal"
      ></TextField>
      <TextField
        fullWidth
        InputProps={{
          readOnly: isOther
        }}
        label="First Name"
        name="first_name"
        onChange={handleFormChange}
        required
        value={data.first_name}
        margin="normal"
      ></TextField>
      <TextField
        fullWidth
        InputProps={{
          readOnly: isOther
        }}
        label="Middle Name"
        name="middle_name"
        onChange={handleFormChange}
        required
        value={data.middle_name}
        margin="normal"
      ></TextField>
      <TextField
        fullWidth
        InputProps={{
          readOnly: isOther
        }}
        label="Last Name"
        name="last_name"
        onChange={handleFormChange}
        required
        value={data.last_name}
        margin="normal"
      ></TextField>
      <TextField
        fullWidth
        InputProps={{
          readOnly: isOther
        }}
        rows="3"
        multiline
        label="Description"
        name="description"
        onChange={handleFormChange}
        margin="normal"
        value={data.description}
      ></TextField>
    </form>
  );
};

export default ProfileForm;
