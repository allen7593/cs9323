import React, { useState, useEffect, Fragment } from "react";
import {
  Grid,
  Paper,
  Card,
  Typography,
  Container,
  CardHeader,
  CardContent,
  Button
} from "@material-ui/core";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { makeStyles } from "@material-ui/styles";
import ProjectList from "./components/ProjectList";
import axios from "axios";
import ProjectForm from "./components/ProjectForm";
import { decode } from "jsonwebtoken";

const useStyles = makeStyles(theme => ({
  root: {},
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  termIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  termBuleIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #4e9bcf, #1976d2)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(25, 118, 210,.4)"
  }
}));

const Project = () => {
  const classes = useStyles();
  const [termData, setTermData] = useState(null);
  const [projectData, setProjectData] = useState([]);
  const [currentProject, setCurrentProject] = useState({});
  const [update, setUpdate] = useState(true);
  useEffect(() => {
    update &&
      axios
        .get("/projects")
        .then(async res => {
          console.log(res);
          await setProjectData(
            res.data.sort(function(a, b) {
              return a["created_date_time"] > b["created_date_time"];
            })
          );
          await setCurrentProject(res.data[0]);
          await setUpdate(false);
        })
        .catch(err => {
          console.log(err);
          setUpdate(false);
        });
  }, [update]);
  const handleUpdate = () => {
    setUpdate(true);
    console.log(update);
  };
  const handleEdit = name => {
    console.log("edit");
    console.log(name);
    console.log(update);
  };

  return (
    <Container maxWidth="lg">
      <Grid container direction="row">
        <Grid item md={12}>
          <Card className={classes.grid}>
            <div className={classes.termIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Project Topic List</b>
                </Typography>
              }
            />
            <CardContent>
              <Grid container justify="space-between" alignItems="flex-start">
                <Grid>
                  <Typography variant="h4" gutterBottom>
                    Current Project: {currentProject.name}
                  </Typography>
                </Grid>
                <Grid>
                  {/* <Button variant="contained" color="secondary">
                    Create Project
                  </Button> */}
                  {decode(localStorage.getItem("x-access-token")).roles[0]
                    .role_name !== "student" && (
                    <ProjectForm update={handleUpdate} />
                  )}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <Grid container direction="row">
        <Grid item md={12} style={{ marginTop: 40 }}>
          <Card className={classes.grid}>
            <div className={classes.termBuleIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Projects</b>
                </Typography>
              }
            />
            <CardContent>
              {console.log(projectData)}
              {projectData != null
                ? projectData.map(item => {
                    return (
                      <ProjectList
                        itemData={item}
                        handleEdit={handleEdit}
                        update={handleUpdate}
                      />
                    );
                  })
                : null}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Project;
