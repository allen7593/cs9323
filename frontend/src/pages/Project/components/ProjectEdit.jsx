import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import axios from 'axios';

function ProjectEdit({ Data,update }) {
    const [open, setOpen] = React.useState(false);
    const [des, setDes] = React.useState(Data.description);
    const [maximum, setMaximum] = React.useState(Data.max_number);
    const [name, setName] = React.useState(Data.name);
    const handleClickOpen = () => {
        setOpen(true);
        console.log(Data);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleSubmit = () => {
        axios.put("/project",
            {
                "id":Data.id,
                "name": name,
                "description": des,
                "max_number": maximum,
            }).then(res => {
                console.log(res);
                setOpen(false);
                update();
            }).catch(error => {
                console.log(error);
            });

    };
    const handleMax = event => {
        setMaximum(parseInt(event.target.value));
        console.log(event.target.value);
        if (event.target.value==="") {
            setMaximum(0);
        }
    };
    const handleDes = (event) => {
        setDes(event.target.value)
    };

    const handleName = event => {
        setName(event.target.value);
    };
    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                Edit
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Project</DialogTitle>
                <DialogContent>
                    <form>
                        <div>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="title"
                                label="Project Name"
                                fullWidth
                                onChange={handleName}
                                value={name}
                            />
                        </div>
                        <div className="">
                            <TextField
                                autoFocus
                                margin="dense"
                                id="maximum"
                                label="maximum people"
                                onChange={handleMax}
                                fullWidth
                                value={maximum}
                            />
                        </div>
                        <div>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="description"
                                label="Project description "
                                onChange={handleDes}
                                fullWidth
                                value={des}
                            />
                        </div>

                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ProjectEdit;