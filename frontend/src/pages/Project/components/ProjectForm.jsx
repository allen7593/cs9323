import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import axios from "axios";

function ProjectForm({ update }) {
    const [open, setOpen] = React.useState(false);
    const [des, setDes] = React.useState("");
    const [maximum, setMaximum] = React.useState(0);
    const [name, setName] = React.useState("test");
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleSubmit = () => {
        axios.post("/project",
            {
                "name": name,
                "description": des,
                "max_number": maximum,
            }).then(res => {
                console.log(res);
                setOpen(false);
                update();
            }).catch(error => {
                console.log(error);
            });
    };
    const handleMax = event => {
        setMaximum(parseInt(event.target.value));
    };
    const handleDis = (event) => {
        setDes(event.target.value)
    };

    const handleName = event => {
        setName(event.target.value);
    };
    return (
        <div>
            <Button variant="contained" color="primary" onClick={handleClickOpen}>
                Create Project
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Project</DialogTitle>
                <DialogContent>
                    <form>
                        <div>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="title"
                                label="Project Name"
                                fullWidth
                                onChange={handleName}
                            />
                        </div>
                        <div className="">
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="maximum people"
                                onChange={handleMax}
                                fullWidth
                            />
                        </div>
                        <div>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Project description "
                                onChange={handleDis}
                                fullWidth
                            />
                        </div>

                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ProjectForm;
