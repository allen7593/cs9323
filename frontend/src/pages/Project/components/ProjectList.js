import React, { useState, useEffect, Fragment } from "react";
import {
  Grid,
  Paper,
  Card,
  Typography,
  Container,
  CardHeader,
  CardContent,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import LensRoundedIcon from "@material-ui/icons/LensRounded";
import dayjs from "dayjs";
import ProjectEdit from "./ProjectEdit";
import { decode } from "jsonwebtoken";

const useStyles = makeStyles(theme => ({
  root: {},
  dotIcon: {
    position: "relative",
    left: -10
  },
  timeRange: {
    fontSize: 20,
    fontWeight: 500,
    position: "relative",
    left: 10,
    top: -5
  },
  termContent: {
    // borderLeft: "3px solid #f27b53",
    padding: theme.spacing(2),
    paddingLeft: theme.spacing(2)
  }
}));

const ProjectList = props => {
  const { itemData, handleEdit, update } = props;
  const classes = useStyles();
  const [data, setData] = useState({
    term_name: "",
    description: "",
    start_date: "",
    end_date: ""
  });
  const [isCurrent, setIsCurrent] = useState(false);

  useEffect(() => {
    const getTermData = async () => {
      await setData({
        ...itemData,
        start_date: dayjs(itemData.start_date).format("DD/MM/YYYY"),
        end_date: dayjs(itemData.end_date).format("DD/MM/YYYY")
      });

      if (
        dayjs(new Date()).isAfter(dayjs(itemData.start_date)) &&
        dayjs(new Date()).isBefore(dayjs(itemData.end_date))
      ) {
        await setIsCurrent(true);
      }
    };
    getTermData();
  }, []);

  return (
    <Paper elevation={0} style={{ margin: "5px 20px 5px 20px" }}>
      {/* <div>
        <LensRoundedIcon className={classes.dotIcon} />
        <span
          className={classes.timeRange}
        >{`${data.start_date} - ${data.end_date}`}</span>
      </div> */}
      <div className={classes.termContent}>
        <Card style={isCurrent ? null : null}>
          <CardContent
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "flex-start"
            }}
          >
            <div>
              <Typography variant="h3" gutterBottom>
                <b>{data.name}</b>
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.description}
              </Typography>
            </div>
            {decode(localStorage.getItem("x-access-token")).roles[0]
              .role_name !== "student" && (
              <ProjectEdit Data={itemData} update={update} />
            )}
          </CardContent>
        </Card>
      </div>
    </Paper>
  );
};

export default ProjectList;
