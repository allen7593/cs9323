import React, { useState, useEffect, Fragment } from "react";
import TeamTable from "./components/TeamTable";
import {
  Grid,
  Card,
  Paper,
  Container,
  Typography,
  Button,
  CardContent,
  CardHeader
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PeopleAltRoundedIcon from "@material-ui/icons/PeopleAltRounded";
import axios from "axios";
import { decode } from "jsonwebtoken";
const useStyles = makeStyles(theme => ({
  root: {},
  title: {
    margin: theme.spacing(2),
    padding: theme.spacing(1)
  },
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  teamIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  teamListIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #4e9bcf, #1976d2)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(25, 118, 210,.4)"
  }
}));

const Team = props => {
  const classes = useStyles();
  const [data, setData] = useState({
    id: "",
    team_name: "",
    description: ""
  });
  const [inTeam, setInTeam] = useState(true);
  const token = decode(localStorage.getItem("x-access-token"));

  useEffect(() => {
    axios
      .get("/current_team")
      .then(res => {
        console.log(res);
        setData({
          ...data,
          id: res.data.id,
          team_name: res.data.name,
          description: res.data.description
        });
      })
      .catch(err => {
        console.log(err);
        if (err.status === 404) {
          setInTeam(false);
        }
      });
  }, []);

  const handleTeamCreateButton = () => {
    props.history.push("/team/create");
  };
  const handleMyTeamButton = () => {
    props.history.push("/team/myteam");
  };

  return (
    <div>
      <Container maxWidth="lg">
        <Grid container direction="row">
          <Grid item md={12}>
            <Card className={classes.grid}>
              <div className={classes.teamIcon}>
                <PeopleAltRoundedIcon />
              </div>
              <CardHeader
                title={
                  <Fragment>
                    <Typography variant="h3" gutterBottom>
                      {token.roles[0]["role_name"] == "student" ? (
                        <b>My Team</b>
                      ) : (
                        <b>Team management</b>
                      )}
                    </Typography>
                    <Grid
                      container
                      justify="space-between"
                      alignItems="flex-end"
                      md={12}
                      style={{ marginTop: 20 }}
                    >
                      {inTeam ? (
                        <Fragment>
                          <Grid item>
                            <Typography variant="h3" gutterBottom>
                              {data.team_name}
                            </Typography>
                            <Typography component="body1">
                              Description: {data.description}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Button
                              variant="contained"
                              color="secondary"
                              onClick={handleMyTeamButton}
                            >
                              View My Team
                            </Button>
                          </Grid>
                        </Fragment>
                      ) : token.roles[0]["role_name"] == "student" ? (
                        <Fragment>
                          <Grid item>
                            <Typography variant="h4" gutterBottom>
                              You are not in a team
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Button
                              variant="contained"
                              color="secondary"
                              onClick={handleTeamCreateButton}
                            >
                              + Create Team
                            </Button>
                          </Grid>
                        </Fragment>
                      ) : null}
                    </Grid>
                  </Fragment>
                }
              />
            </Card>
          </Grid>
        </Grid>
        <Grid></Grid>
        <Grid container direction="row" style={{ marginTop: 30 }}>
          <Grid item md={12}>
            <Card className={classes.grid}>
              <div className={classes.teamIcon}>
                <PeopleAltRoundedIcon />
              </div>
              <CardHeader
                title={
                  <Typography variant="h3" gutterBottom>
                    <b>Teams</b>
                  </Typography>
                }
              />
              <CardContent>
                <TeamTable {...props} data={data} inTeam={inTeam} />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Team;
