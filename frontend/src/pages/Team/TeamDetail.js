import React, { useState, useEffect, Fragment } from "react";
import {
  Grid,
  Card,
  Paper,
  Container,
  Typography,
  Button,
  CardContent,
  CardHeader,
  CardActions,
  Divider
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PeopleAltRoundedIcon from "@material-ui/icons/PeopleAltRounded";
import TeamForm from "./components/TeamForm";
import MemberTable from "./components/MemberTable";
import MentorTable from "./components/MentorTable";
import axios from "axios";
import { decode } from "jsonwebtoken";

const useStyles = makeStyles(theme => ({
  root: {},
  title: {
    margin: theme.spacing(2),
    padding: theme.spacing(1)
  },
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  teamIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  teamListIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #4e9bcf, #1976d2)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(25, 118, 210,.4)"
  }
}));

const TeamDetail = props => {
  const classes = useStyles();
  const url_state = props.match.params.team_id;
  const [headData, setHeadData] = useState({ name: "", description: "" });
  const token = decode(localStorage.getItem("x-access-token"));

  useEffect(() => {
    url_state == "myteam" &&
      axios
        .get(`/current_team`)
        .then(res => {
          console.log(res);
          setHeadData({
            name: res.data.name,
            description: res.data.description
          });
        })
        .catch(err => {
          console.log(err);
        });
    url_state !== "create" &&
      url_state !== "myteam" &&
      axios
        .get(`/team/${url_state}`)
        .then(res => {
          console.log(res);
          setHeadData({
            name: res.data.name,
            description: res.data.description
          });
        })
        .catch(err => {
          console.log(err);
        });
  }, []);

  const handleTeamSave = () => {};

  const handleTeamDelete = () => {};

  return (
    <div>
      <Container maxWidth="lg">
        <Grid container direction="row">
          <Grid item md={12}>
            <Card className={classes.grid}>
              <div className={classes.teamIcon}>
                <PeopleAltRoundedIcon />
              </div>
              <CardHeader
                title={
                  <Fragment>
                    <Typography variant="h3" gutterBottom>
                      {token.roles[0]["role_name"] == "student" ? (
                        <b>My Team</b>
                      ) : (
                        <b>Team management</b>
                      )}
                    </Typography>
                    <Grid
                      container
                      justify="space-between"
                      alignItems="flex-end"
                      md={12}
                      style={{ marginTop: 20 }}
                    >
                      {url_state === "create" ? (
                        <Grid item>
                          <Typography variant="h3" gutterBottom>
                            Create your team
                          </Typography>
                        </Grid>
                      ) : url_state === "myteam" ? (
                        <Fragment>
                          <Grid item>
                            <Typography variant="h3" gutterBottom>
                              {headData.name}
                            </Typography>
                            <Typography component="body1">
                              {headData.description}
                            </Typography>
                          </Grid>
                        </Fragment>
                      ) : token.roles[0]["role_name"] == "student" ? (
                        <Fragment>
                          <Grid item>
                            <Typography variant="h3" gutterBottom>
                              Join a team
                            </Typography>
                          </Grid>
                        </Fragment>
                      ) : null}
                    </Grid>
                  </Fragment>
                }
              />
            </Card>
          </Grid>
        </Grid>
        <Grid container style={{ marginTop: 30 }}>
          <Grid item md={8} sm={12}>
            <Card className={classes.grid}>
              <div className={classes.teamIcon}>
                <PeopleAltRoundedIcon />
              </div>
              <CardHeader
                title={
                  <Typography variant="h3" gutterBottom>
                    Details
                  </Typography>
                }
              />
              <CardContent>
                <TeamForm {...props} url_state={url_state} />
              </CardContent>
            </Card>
          </Grid>
          <Grid container direction="column" item md={4} sm={12}>
            <Grid item>
              <Card className={classes.grid}>
                <div className={classes.teamIcon}>
                  <PeopleAltRoundedIcon />
                </div>
                <CardHeader
                  title={
                    <Typography variant="h3" gutterBottom>
                      Project Members
                    </Typography>
                  }
                />
                <CardContent>
                  <MemberTable url_state={url_state} />
                </CardContent>
              </Card>
            </Grid>
            <Grid item style={{ marginTop: 30 }}>
              <Card className={classes.grid}>
                <div className={classes.teamIcon}>
                  <PeopleAltRoundedIcon />
                </div>
                <CardHeader
                  title={
                    <Typography variant="h3" gutterBottom>
                      Project Mentor Information
                    </Typography>
                  }
                />
                <CardContent>
                  <MentorTable url_state={url_state} />
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default TeamDetail;
