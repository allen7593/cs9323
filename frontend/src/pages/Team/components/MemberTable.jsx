import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "@material-ui/core";

import axios from "axios";

function createData(team_id, team_name, team_member, team_description) {
  return { team_id, team_name, team_member, team_description };
}

const rows = [
  createData(
    "sajd81",
    "Frozen yoghurt",
    4,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "sajd231",
    "Ice cream sandwich",
    3,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "sajd23",
    "Eclair",
    2,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "saj34",
    "Cupcake",
    4,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "saj1421",
    "Gingerbread",
    5,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  )
];

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    overflowX: "auto"
  },

  button: {
    margin: theme.spacing(1),
    width: "100%"
  }
}));

const MemberTable = props => {
  const classes = useStyles();
  const { url_state } = props;
  console.log(url_state);
  const [data, setData] = useState({
    team_name: "",
    members: [{ id: "11", first_name: "11", email_address: "11" }]
  });

  const handleTeamView = id => {
    props.history.push(`/team/${id}`);
  };

  useEffect(() => {
    if (url_state === "create") {
      setData({
        team_name: "",
        members: [{ id: "", first_name: "", email_address: "" }]
      });
      console.log(data);
    } else if (url_state === "myteam") {
      axios
        .get("/current_team")
        .then(res => {
          console.log(res);
          setData({
            ...data,
            id: res.data.id,
            team_name: res.data.name,
            members: res.data.team_members
          });
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      axios.get(`/team/${url_state}`).then(res => {
        console.log(res);
        // setData({
        //   team_id: res.data.id,
        //   team_name: res.data.name,
        //   team_member: res.data.team_members.length,
        //   team_description: res.data.description
        // });
        setData({
          ...data,
          id: res.data.id,
          team_name: res.data.name,
          members: res.data.team_members
        });
      });
    }
  }, []);
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="simple table" size="small">
        <TableHead>
          <TableRow>
            <TableCell align="center">Member Name</TableCell>
            <TableCell align="center">Email</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {console.log(data.members)}
          {data.members.length === 0
            ? null
            : data.members.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    <Link to={`/profile/${row.id}`}>{row.first_name}</Link>
                  </TableCell>
                  <TableCell align="left">{row.email_address}</TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>
    </Paper>
  );
};
export default MemberTable;
