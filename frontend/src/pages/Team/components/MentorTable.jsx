import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Grid
} from "@material-ui/core";
import { decode } from "jsonwebtoken";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    overflowX: "auto"
  },

  button: {
    margin: theme.spacing(1),
    width: "100%"
  }
}));

const MemberTable = props => {
  const classes = useStyles();
  const { url_state } = props;
  const [mentorList, setMentorList] = useState([]);
  const [buttonVisible, setButtonVisible] = useState(false);
  const [data, setData] = useState({
    team_id: "",
    assignee: { id: "" }
  });

  useEffect(() => {
    if (url_state === "create") {
      setData({
        team_id: "",
        assignee: { id: "" }
      });
      console.log(data);
    } else if (url_state === "myteam") {
      axios
        .get("/current_team")
        .then(async res => {
          console.log(res);
          await setData(res.data);
          await axios
            .get("/user")
            .then(async res => {
              console.log(res);
              let mentor_list = [];

              await res.data.map(item => {
                if (item.roles[0].description === "mentor") {
                  mentor_list.push(item);
                }
              });
              console.log(mentor_list);
              await setMentorList(mentor_list);
            })
            .catch(err => {
              console.log(err);
            });
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      axios.get(`/team/${url_state}`).then(async res => {
        console.log(res);
        await setData(res.data);
        await axios
          .get("/user")
          .then(async res => {
            console.log(res);
            let mentor_list = [];

            await res.data.map(item => {
              if (item.roles[0].description === "mentor") {
                mentor_list.push(item);
              }
            });
            console.log(mentor_list);
            await setMentorList(mentor_list);
          })
          .catch(err => {
            console.log(err);
          });
        const role = decode(localStorage.getItem("x-access-token")).roles;
        console.log(role);
        if (
          Object.keys(res.data.assignee).length === 0 &&
          role[0].description !== "student"
        ) {
          console.log("asdasd");
          setButtonVisible(true);
        }
      });
    }
  }, [buttonVisible]);

  const handleChange = event => {
    setData({ ...data, assignee: { id: event.target.value } });
  };

  const handleAssign = () => {
    console.log(data);
    axios
      .put("/team", data)
      .then(res => {
        console.log(res);
        setButtonVisible(false);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <Paper className={classes.root}>
      {buttonVisible ? (
        <Grid
          container
          justify="flex-end"
          alignItems="center"
          style={{ padding: 10 }}
        >
          <Grid item md={8} style={{ marginBottom: 10 }}>
            <FormControl
              className={classes.formControl}
              style={{ width: "100%" }}
            >
              <InputLabel id="demo-simple-select-label">Mentor</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={(() => {
                  let res = mentorList.filter(
                    item => item.id === data.assignee.id
                  );
                })()}
                onChange={handleChange}
              >
                {mentorList.map(item => (
                  <MenuItem value={item.id} key={item.id}>
                    {item.first_name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item md={4}>
            <Button color="secondary" onClick={handleAssign}>
              Assign
            </Button>
          </Grid>
        </Grid>
      ) : null}

      <Table className={classes.table} aria-label="simple table" size="small">
        <TableHead>
          <TableRow>
            <TableCell align="center">Mentor Name</TableCell>
            <TableCell align="center">Email</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {mentorList.map(item => {
            if (item.id === data.assignee.id)
              return (
                <TableRow>
                  <TableCell component="th" scope="row">
                    <Link to={`/profile/${item.id}`}>{item.first_name}</Link>
                  </TableCell>
                  <TableCell align="left">{item.email_address}</TableCell>
                </TableRow>
              );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};
export default MemberTable;
