import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  TextField,
  Checkbox,
  FormControlLabel,
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  Grid,
  Typography,
  Divider,
  LinearProgress
} from "@material-ui/core";
import Axios from "axios";
import { decode } from "jsonwebtoken";
import SnackBar from "../../../components/Snackbar";

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(2),
    padding: theme.spacing(1)
  },
  button: {
    margin: theme.spacing(2)
  },
  formControl: {
    display: "block"
  },
  margin: {
    height: theme.spacing(1),
    margin: theme.spacing(4.5)
  }
}));

function TeamForm(props) {
  const classes = useStyles();
  const { user_id } = decode(localStorage.getItem("x-access-token"));
  const { url_state } = props;
  const [open, setOpen] = useState(false);
  const [snack, setSnack] = useState({ type: "success", msg: "" });
  const [refresh, setRefresh] = useState(true);
  const [select, setSelect] = useState({});
  const [vote, setVote] = useState({});
  const [formData, setFormData] = useState({
    description: "",
    name: "",
    projects: [],
    team_members: [],
    vote: { tickets: [] },
    assign: { id: "" }
  });

  const [projectData, setProjectData] = useState([
    {
      id: "",
      name: ""
    }
  ]);

  //   for (let i = 0; i < projectData.length; i++) {
  //     let name = projectData[i].name;
  //     setSelect({ ...select, [name]: false });
  //   }sd

  useEffect(() => {
    if (refresh === true) {
      switch (url_state) {
        case "create":
          setFormData({
            assign: { id: "" },
            description: "",
            name: "",
            projects: [],
            vote: { tickets: [] }
          });
          Axios.get("/projects")
            .then(async res => {
              console.log(res);
              setProjectData(res.data);
              setRefresh(false);
            })
            .catch(err => {
              console.log(err);
              setRefresh(false);
            });
          break;
        case "myteam":
          Axios.get("/current_team")
            .then(res => {
              console.log(res);
              setFormData({
                ...res.data,
                id: res.data.id,
                name: res.data.name,
                team_members: res.data.team_members,
                description: res.data.description,
                vote: res.data.vote
              });
              setProjectData(res.data.projects);
              setRefresh(false);
            })
            .catch(err => {
              console.log(err);
              setRefresh(false);
            });

          break;
        default:
          Axios.get(`/team/${url_state}`)
            .then(res => {
              console.log(res);
              let checked_obj = {};
              res.data.projects.map(obj => {
                checked_obj = { ...checked_obj, [obj.id]: true };
              });
              console.log(checked_obj);
              setSelect(checked_obj);
              setFormData({
                ...res.data,
                id: res.data.id,
                name: res.data.name,
                team_members: res.data.team_members,
                description: res.data.description,
                vote: res.data.vote
              });

              setRefresh(false);
            })
            .then(() => {
              Axios.get("/projects")
                .then(res => {
                  console.log(res);
                  setProjectData(res.data);

                  setRefresh(false);
                })
                .catch(err => {
                  console.log(err);
                  setRefresh(false);
                });
            })
            .catch(err => {
              console.log(err);
              setRefresh(false);
            });

          break;
      }
    }
  }, [refresh, url_state]);

  const handleFormChange = event => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });
    console.log(formData);
    console.log(select);
  };

  const handleSelectChange = name => event => {
    setSelect({ ...select, [name]: event.target.checked });
  };

  const handleTeamSave = () => {
    console.log(formData);
    console.log(select);
    let select_list = Object.keys(select);
    let proj_list = [];
    for (let i = 0; i < select_list.length; i++) {
      if (select[select_list[i]] === true) {
        proj_list.push({ id: select_list[i] });
      }
    }
    console.log(proj_list);
    let team_obj = {
      ...formData,
      projects: proj_list
    };
    console.log(team_obj);
    Axios.post("/team", team_obj)
      .then(res => {
        console.log(res);
        setSnack({ type: "success", msg: "Successfully Created Team!" });
        setOpen(true);
        props.history.push("/team");
      })
      .catch(err => {
        console.log(err);
        setSnack({ type: "warning", msg: "Creating Team Failed!" });
        setOpen(true);
      });
  };

  const handleTeamCancel = () => {
    props.history.push("/team");
  };

  const handleTeamVote = () => {
    console.log("vote");
    console.log(select);
    let select_list = Object.keys(select);
    let vote_proj_list = [];
    for (let i = 0; i < select_list.length; i++) {
      if (select[select_list[i]] === true) {
        vote_proj_list.push({ project_id: select_list[i] });
      }
    }
    console.log(vote_proj_list);

    Axios.post(`/vote/${formData.vote.id}`, vote_proj_list)
      .then(res => {
        console.log(res);
        setSnack({ type: "success", msg: "Successfully Voted!" });
        setOpen(true);
        setRefresh(true);
      })
      .catch(err => {
        console.log(err);
        setSnack({ type: "warning", msg: "Voting Failed!" });
        setOpen(true);
        setRefresh(true);
      });
  };

  const handleTeamJoin = () => {
    console.log("join");
    let new_member = formData.team_members;
    new_member.map(obj => {
      if (user_id === obj.id) {
        setSnack({ type: "warning", msg: "Already in this team!" });
        setOpen(true);
        return;
      }
    });
    new_member.push({ id: user_id });
    console.log(new_member);
    Axios.put("/team", {
      ...formData,
      team_members: new_member
    })
      .then(res => {
        console.log(res);
        setSnack({ type: "success", msg: "Successfully Joined Team!" });
        setOpen(true);
        props.history.push("/team");
      })
      .catch(err => {
        console.log(err);
        setSnack({ type: "warning", msg: "Joining Team Failed!" });
        setOpen(true);
      });
  };

  const handleTeamLeave = () => {
    console.log("leave");
    let new_member = formData.team_members.filter(
      member => member.id !== user_id
    );
    console.log(formData.team_members);
    console.log(new_member);
    Axios.put("/team", {
      ...formData,
      team_members: new_member
    })
      .then(res => {
        console.log(res);
        setSnack({ type: "success", msg: "Leaved Team!" });
        setOpen(true);
        props.history.push("/team");
      })
      .catch(err => {
        console.log(err);
        setSnack({ type: "warning", msg: "Error!" });
        setOpen(true);
      });
  };
  const handleSnack = bool => {
    setOpen(bool);
  };
  return (
    <form className={classes.form}>
      <Grid container>
        <Grid item md={12} sm={12}>
          <TextField
            fullWidth
            // InputProps={{
            //   readOnly: false
            // }}
            onChange={handleFormChange}
            label="Team Name"
            name="name"
            value={formData.name}
            margin="normal"
          ></TextField>
        </Grid>
        <Grid item md={12} sm={12}>
          <TextField
            fullWidth
            label="Description"
            name="description"
            onChange={handleFormChange}
            margin="normal"
            value={formData.description}
          ></TextField>
        </Grid>
        <Grid item md={12} sm={12}>
          <FormControl className={classes.formControl}>
            <FormLabel component="legend">
              <Typography variant="h4" gutterBottom style={{ marginTop: 20 }}>
                {url_state === "myteam"
                  ? "Vote Projects"
                  : "Project Preference"}
              </Typography>
            </FormLabel>
            <Grid container justify="center" alignItems="center">
              {url_state === "myteam" ? (
                <Fragment>
                  <Grid item md={7}>
                    <FormGroup style={{ margin: "20px 0 20px 20px" }}>
                      {projectData.map((project, index) => {
                        return (
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={select[project.id]}
                                onChange={handleSelectChange(project.id)}
                                value={project.id}
                              />
                            }
                            label={project.name}
                          />
                        );
                      })}
                    </FormGroup>
                  </Grid>

                  <Grid item md={5}>
                    {projectData.map(obj => {
                      let count = 0;
                      if (formData.vote.tickets !== undefined) {
                        for (let i = 0; i < formData.vote.tickets.length; i++) {
                          if (formData.vote.tickets[i].project_id === obj.id) {
                            count += 1;
                          }
                        }
                      }

                      return (
                        <LinearProgress
                          variant="determinate"
                          value={20 * count}
                          thickness={4}
                          color="secondary"
                          className={classes.margin}
                        />
                      );
                    })}
                  </Grid>
                </Fragment>
              ) : (
                <Fragment>
                  <Grid item md={6}>
                    <FormGroup style={{ margin: 20 }}>
                      {projectData.slice(0, 5).map((project, index) => {
                        return (
                          <FormControlLabel
                            control={
                              <Checkbox
                                disabled={url_state === "create" ? false : true}
                                checked={select[project.id]}
                                onChange={handleSelectChange(project.id)}
                                value={project.id}
                              />
                            }
                            label={project.name}
                          />
                        );
                      })}
                    </FormGroup>
                  </Grid>
                  <Grid item md={6}>
                    <FormGroup style={{ margin: 20 }}>
                      {projectData.slice(5, 10).map((project, index) => {
                        return (
                          <FormControlLabel
                            control={
                              <Checkbox
                                disabled={url_state === "create" ? false : true}
                                checked={select[project.id]}
                                onChange={handleSelectChange(project.id)}
                                value={project.id}
                              />
                            }
                            label={project.name}
                          />
                        );
                      })}
                    </FormGroup>
                  </Grid>
                </Fragment>
              )}
            </Grid>
          </FormControl>
        </Grid>
        <Grid item md={12} sm={12}>
          <Divider />
        </Grid>
        {url_state === "myteam" ? (
          <Grid item md={12} sm={12}>
            {(() => {
              console.log(formData.vote.tickets);
              if (formData.vote.tickets !== undefined) {
                for (let i = 0; i < formData.vote.tickets.length; i++) {
                  if (formData.vote.tickets[i].user.id === user_id) {
                    return null;
                  }
                }
              } else {
                return (
                  <Button
                    className={classes.button}
                    variant="contained"
                    color="secondary"
                    onClick={handleTeamVote}
                  >
                    Vote
                  </Button>
                );
              }
            })()}
            <Button
              className={classes.button}
              variant="outlined"
              color="primary"
              onClick={handleTeamLeave}
            >
              Leave
            </Button>
          </Grid>
        ) : url_state === "create" ? (
          <Grid item md={12} sm={12}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={handleTeamSave}
            >
              + Create
            </Button>
            <Button
              className={classes.button}
              variant="outlined"
              color="primary"
              onClick={handleTeamCancel}
            >
              Cancel
            </Button>
          </Grid>
        ) : (
          (() => {
            let list = Object.values(formData.team_members);
            if (list.indexOf(user_id) === -1) {
              return (
                <Grid item md={12} sm={12}>
                  <Button
                    className={classes.button}
                    variant="contained"
                    color="secondary"
                    onClick={handleTeamJoin}
                  >
                    + Join
                  </Button>
                </Grid>
              );
            } else {
              return null;
            }
          })()
        )}
      </Grid>
      <SnackBar
        type={snack.type}
        msg={snack.msg}
        open={open}
        handleOpen={handleSnack}
      />
    </form>
  );
}

export default TeamForm;
