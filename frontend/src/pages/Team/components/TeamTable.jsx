import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "@material-ui/core";

import axios from "axios";
import { decode } from "jsonwebtoken";
function createData(team_id, team_name, team_member, team_description) {
  return { team_id, team_name, team_member, team_description };
}

const rows = [
  createData(
    "sajd81",
    "Frozen yoghurt",
    4,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "sajd231",
    "Ice cream sandwich",
    3,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "sajd23",
    "Eclair",
    2,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "saj34",
    "Cupcake",
    4,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  ),
  createData(
    "saj1421",
    "Gingerbread",
    5,
    "explained that, unlike real paper, our digital material can expand and reform intelligently. Material has physical surfaces and edges. Seamsand shadows provide meaning about what you can touch"
  )
];

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  button: {
    margin: theme.spacing(1),
    width: "100%"
  }
}));

const TeamTable = props => {
  const classes = useStyles();
  const [data, setData] = useState([
    {
      team_id: "",
      team_name: "",
      team_members: "",
      team_description: ""
    }
  ]);
  const handleTeamView = id => {
    if (props.data.id === id) {
      props.history.push(`/team/myteam`);
    } else {
      props.history.push(`/team/${id}`);
    }
  };
  const token = decode(localStorage.getItem("x-access-token"));
  useEffect(() => {
    axios
      .get("/team")
      .then(res => {
        console.log(res);
        // setData({
        //   team_id: res.data.id,
        //   team_name: res.data.name,
        //   team_member: res.data.team_members.length,
        //   team_description: res.data.description
        // });
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Team Name</TableCell>
            <TableCell align="center">Members</TableCell>
            <TableCell align="center">Description</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="center">{row.team_members.length}</TableCell>
              <TableCell align="left">{row.description}</TableCell>
              <TableCell align="center">
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  onClick={() => handleTeamView(row.id)}
                >
                  {token.roles[0]["role_name"] === "student" && props.inTeam
                    ? "View"
                    : "Join"}
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};
export default TeamTable;
