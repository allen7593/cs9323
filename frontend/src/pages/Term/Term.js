import React, { useState, useEffect, Fragment } from "react";
import {
  Grid,
  Paper,
  Card,
  Typography,
  Container,
  CardHeader,
  CardContent,
  Button
} from "@material-ui/core";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { makeStyles } from "@material-ui/styles";

import TermEvent from "./components/TermEvent";
import axios from "axios";
import { decode } from "jsonwebtoken";
const useStyles = makeStyles(theme => ({
  root: {},
  grid: {
    margin: theme.spacing(1),
    overflow: "visible"
  },
  termIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #35cce6, #77c6da)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)"
  },
  termBuleIcon: {
    borderRadius: 4,
    float: "left",
    padding: 15,
    margin: "-15px auto auto 15px",
    color: "white",
    background: "linear-gradient(60deg, #4e9bcf, #1976d2)",
    boxShadow:
      "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(25, 118, 210,.4)"
  }
}));

const Term = () => {
  const classes = useStyles();
  const [termData, setTermData] = useState(null);
  useEffect(() => {
    axios.get("/term").then(res => {
      console.log(res);
      setTermData(res.data);
    });
  }, []);

  const handleEdit = name => {
    console.log("edit");
    console.log(name);
  };

  return (
    <Container maxWidth="lg">
      <Grid container direction="row">
        <Grid item md={12} sm={12}>
          <Card className={classes.grid}>
            <div className={classes.termIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Term Management</b>
                </Typography>
              }
            />
            <CardContent>
              <Grid container justify="space-between" alignItems="flex-start">
                <Grid>
                  <Typography variant="h4" gutterBottom>
                    Current Term: 19T3
                  </Typography>
                </Grid>
                <Grid>
                  {decode(localStorage.getItem("x-access-token")).roles[0]
                    .role_name !== "student" && (
                    <Button variant="contained" color="secondary">
                      Create Term
                    </Button>
                  )}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <Grid container direction="row">
        <Grid item md={12} sm={12} style={{ marginTop: 40 }}>
          <Card className={classes.grid}>
            <div className={classes.termBuleIcon}>
              <LibraryBooksIcon />
            </div>
            <CardHeader
              title={
                <Typography variant="h3" gutterBottom>
                  <b>Terms</b>
                </Typography>
              }
            />
            <CardContent>
              {console.log(termData)}
              {termData != null
                ? termData.map((item, index) => {
                    return (
                      <TermEvent termData={item} handleEdit={handleEdit} />
                    );
                  })
                : null}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Term;
