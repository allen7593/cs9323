import React, { useState, useEffect, Fragment } from "react";
import {
  Grid,
  Paper,
  Card,
  Typography,
  Container,
  CardHeader,
  CardContent,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import LensRoundedIcon from "@material-ui/icons/LensRounded";
import dayjs from "dayjs";
import { decode } from "jsonwebtoken";

const useStyles = makeStyles(theme => ({
  root: {},
  dotIcon: {
    position: "relative",
    left: -10
  },
  timeRange: {
    fontSize: 20,
    fontWeight: 500,
    position: "relative",
    left: 10,
    top: -5
  },
  termContent: {
    borderLeft: "3px solid #f27b53",
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(4),
    paddingBottom: theme.spacing(5)
  }
}));

const TermEvent = props => {
  const { termData, handleEdit } = props;
  const classes = useStyles();
  const [data, setData] = useState({
    term_name: "",
    description: "",
    start_date: "",
    end_date: ""
  });
  const [isCurrent, setIsCurrent] = useState(false);

  useEffect(() => {
    const getTermData = async () => {
      await setData({
        ...termData,
        start_date: dayjs(termData.start_date).format("DD/MM/YYYY"),
        end_date: dayjs(termData.end_date).format("DD/MM/YYYY")
      });

      if (
        dayjs(new Date()).isAfter(dayjs(termData.start_date)) &&
        dayjs(new Date()).isBefore(dayjs(termData.end_date))
      ) {
        await setIsCurrent(true);
      }
    };
    getTermData();
  }, []);

  return (
    <Paper elevation={0} style={{ margin: "5px 20px 5px 20px" }}>
      <div>
        <LensRoundedIcon className={classes.dotIcon} />
        <span
          className={classes.timeRange}
        >{`${data.start_date} - ${data.end_date}`}</span>
      </div>
      <div className={classes.termContent}>
        <Card style={isCurrent ? { border: "2px solid #f27b53" } : null}>
          <CardContent
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "flex-start"
            }}
          >
            <div>
              <Typography variant="h3" gutterBottom>
                <b>{data.term_name}</b>
              </Typography>
              <Typography variant="body1" gutterBottom>
                {data.description}
              </Typography>
            </div>
            {decode(localStorage.getItem("x-access-token")).roles[0]
              .role_name !== "student" && (
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  handleEdit(data.term_name);
                }}
              >
                EDIT
              </Button>
            )}
          </CardContent>
        </Card>
      </div>
    </Paper>
  );
};

export default TermEvent;
