import React, { Suspense, lazy, useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import RouteWithLayout from "./components/RouteWithLayout";
import Loading from "./components/Loading";
import { DashboardLayout } from "./layouts/Dashboard/";
import Dashboard from "./pages/Dashboard";
import { Team, TeamDetail } from "./pages/Team";
import { Notification, NotificationDetail } from "./pages/Notification";
import Profile from "./pages/Profile";

import Project from "./pages/Project";
import Term from "./pages/Term";
import NotFound from "./pages/404";
// const Dashboard = lazy(() => import("./pages/Dashboard"));
const Login = lazy(() => import("./pages/Login"));

const Routes = () => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route component={Login} exact path="/login" />
        <Redirect exact from="/" to="/dashboard" />
        <RouteWithLayout
          component={Dashboard}
          exact
          layout={DashboardLayout}
          path="/dashboard"
        />
        <RouteWithLayout
          component={Team}
          exact
          layout={DashboardLayout}
          path="/team"
        />
        <RouteWithLayout
          component={TeamDetail}
          exact
          layout={DashboardLayout}
          path="/team/:team_id"
        />
        <RouteWithLayout
          component={Notification}
          exact
          layout={DashboardLayout}
          path="/notification"
        />
        <RouteWithLayout
          component={NotificationDetail}
          exact
          layout={DashboardLayout}
          path="/notification/:notice_id"
        />
        <RouteWithLayout
          component={Profile}
          exact
          layout={DashboardLayout}
          path="/profile/"
        />
        <RouteWithLayout
          component={Profile}
          exact
          layout={DashboardLayout}
          path="/profile/:user_id"
        />
        <RouteWithLayout
          component={Term}
          exact
          layout={DashboardLayout}
          path="/term"
        />
        <RouteWithLayout
          component={Project}
          exact
          layout={DashboardLayout}
          path="/project"
        />
        <RouteWithLayout
          component={NotFound}
          exact
          layout={DashboardLayout}
          path="/404"
        />
        <Redirect to="/404" />
      </Switch>
    </Suspense>
  );
};
export default Routes;
