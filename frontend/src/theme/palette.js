import { colors } from "@material-ui/core";
import color from "@material-ui/core/colors/amber";

const white = "#FFFFFF";
const black = "#000000";
export default {
  black,
  white,
  primary: {
    contrastText: white,
    dark: "#3f9292",
    main: "#35cce6",
    light: "#7bdada"
  },
  secondary: {
    contrastText: white,
    dark: "#a9563a",
    main: "#f27b53",
    light: "#f49575"
  },
  orange: {
    contrastText: white,
    dark: "#a9563a",
    main: "#f27b53",
    light: "#f49575"
  },
  blue: {
    contrastText: white,
    main: "#4e9bcf"
  },
  pink: {
    contrastText: white,
    dark: colors.pink[900],
    main: colors.pink[600],
    light: colors.pink[400]
  },
  error: {
    contrastText: white,
    dark: colors.red[900],
    main: colors.red[600],
    light: colors.red[400]
  },
  text: {
    primary: colors.blueGrey[900],
    secondary: colors.blueGrey[600],
    link: colors.blue[600],
    white: white
  },
  link: colors.blue[800],
  icon: colors.blueGrey[600],
  background: {
    default: "#F4F6F8",
    paper: white
  },
  divider: colors.grey[200]
};
