import axios from "axios";
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.timeout = 10000;

axios.interceptors.request.use(
  config => {
    // Do something before request is sent
    var token = localStorage.getItem("x-access-token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  error => {
    // Do something with request error
    return Promise.resolve(error); //change from reject to resolve for debugging
  }
);

axios.interceptors.response.use(
  response => {
    console.log(response);
    return response;
  },
  error => {
    // if refresh token expired
    // if (error.response === undefined) {
    //   localStorage.setItem("x-access-token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjb21wOTMyMyIsImlhdCI6MTU3MzQ3OTI5NywiZXhwIjoxNTczNTM5Mjk3LCJzdWIiOiJqbmFycmFtb3JlMUBpbmRpZWdvZ28uY29tIiwidXNlcl9pZCI6IjVkYzNkOTQ1ZGZiNjZiZjU5MWVhMTMxNyJ9.M0sUes4i-fDwlI4LG7yJxghWmrvm1XZugxI5Nvg5sRM");
    // }
    console.log("this is error", error);
    try {
      if (error.response.status === 444) {
        window.location.href = "/login";
      }
    } catch (err) {
      window.location.href = "/login";
    }

    return Promise.reject(error.response);
  }
);
