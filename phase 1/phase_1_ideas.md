# Current Ideas that been discussed in 25/09/2019 meeting
Please read phase 1 guideline on Piazza
Dead line will be 08/10/2019 @ 23:59

Please edit the document on https://docs.google.com/document/d/1PwO5eUi3VX_yY8zgsBJSbFbFaInXUFFdyr-T4pr74kQ/edit?usp=sharing

## Project should address one or more of the following problem
- Team formation
- Marking Process (assignment collection/ assignment distribution in between mentors/ making collection/ marking collection and marking distribution to student)
- Communication (communication in between students/mentors, Q&A)
- Mentoring session scheduling
- Notification (Student/Mentor/phase scheduling notification)


## Point of focus
### After the discussion we agree that we might will use following points as primary focus instead of do all the above points
- Team forming
- Notification
### We will also put the following point(s) as additional focus if there are enough time to finish the primary focus
- Commnications
