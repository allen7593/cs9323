from docx import Document

from senario import Senario


class Feature:
    name: str
    as_a: str
    so_that: str
    i_want_to: str
    senario: Senario
    story_num: int
    feature_num: int

    def __init__(self, name: str, as_a: str, so_that: str, i_want_to: str, story_num: int, feature_num: int):
        self.name = name
        self.as_a = as_a
        self.so_that = so_that
        self.i_want_to = i_want_to
        self.story_num = story_num
        self.feature_num = feature_num

    def __call__(self, senario: Senario):
        self.senario = senario

    def __str__(self):
        return "\n".join(
            [
                f"{self.story_num}.{self.feature_num}: ",
                f"FEATURE {self.name}",
                f"AS A {self.as_a}",
                f"I WANT TO {self.i_want_to}",
                f"SO THAT {self.so_that}",
            ]
        ) + "\n\n" + str(self.senario)

    def write_docx(self, doc: Document):
        doc.add_heading(f"{self.story_num}.{self.feature_num}: {self.name}", level=2)

        p = doc.add_paragraph()
        p.add_run("FEATURE ").bold = True
        r = p.add_run(f"{self.name}")
        r.add_break()

        p.add_run("AS A ").bold = True
        r = p.add_run(f"{self.as_a}")
        r.add_break()

        p.add_run("I WANT TO ").bold = True
        r = p.add_run(f"{self.so_that}")
        r.add_break()

        p.add_run("SO THAT ").bold = True
        r = p.add_run(f"{self.i_want_to}")
        r.add_break()

        self.senario.write_docx(doc)

