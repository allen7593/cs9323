from openpyxl import load_workbook
import requests
from feature import Feature
from senario import Senario
from story import Story
from docx import Document


def write_story(current_story: Story):
    with open("output.txt", "a") as writer:
        writer.write(str(current_story) + "\n")


if __name__ == '__main__':
    resp = requests.get("https://docs.google.com/spreadsheets/d/1jvM8Xhq3tgKMI5bc86MQVgp8miOgonjt2eSyWUiRiVw/export?format=xlsx&id=1jvM8Xhq3tgKMI5bc86MQVgp8miOgonjt2eSyWUiRiVw")
    with open("Features.xlsx", "wb") as writer:
        writer.write(resp.content)
    workbook = load_workbook(filename="Features.xlsx")
    doc = Document()
    current_sheet = workbook.get_sheet_by_name("Features updated")
    print("Active sheet: " + str(current_sheet.title))
    max_rows = current_sheet.max_row
    current_story = None
    current_feature = None
    current_senario = None
    found_story = False
    found_feature = False
    found_senario = False
    story_num = 1
    feature_num = 1
    print("Max line: " + str(max_rows))
    for value in current_sheet.iter_rows(min_row=1, max_row=max_rows, values_only=True):
        if not value[1]:
            continue
        if found_feature:
            found_feature = False
            current_feature = Feature(value[0], value[1], value[2], value[3], story_num, feature_num)
            feature_num += 1
            continue
        elif found_senario:
            found_senario = False
            current_senario = Senario(value[1], value[2], value[3], value[4], value[5], value[6])
            current_feature(current_senario)
            current_story(current_feature)
            continue
        if "Story" in value[0] or "story" in value[0]:
            if current_story:
                print("Writing Story: " + current_story.name)
                write_story(current_story)
                current_story.write_docx(doc)
                story_num += 1
                feature_num = 1
            current_story = Story(value[1], story_num)
        elif ("Features" in value[0] or "features" in value[0]) and not found_feature:
            found_feature = True
        elif ("Senario" in value[0] or "senario" in value[0]) and not found_senario:
            found_senario = True
    print("Writing Story: " + current_story.name)
    write_story(current_story)
    current_story.write_docx(doc)
    doc.save("Features.docx")
