from docx import Document


class Senario:
    given: str
    when1: str
    then1: str
    when2: str
    and1: str
    then2:str

    def __init__(self, given: str, when1: str, then1: str, when2: str, and1:str, then2: str):
        self.given = given
        self.when1 = when1
        self.then1 = then1
        self.when2 = when2
        self.and1 = and1
        self.then2 = then2

    def __str__(self):
        return "\n".join(
            [
                "Scenario:",
                f"GIVEN {self.given}",
                f"WHEN {self.when1}",
                f"THEN {self.then1}",
                f"WHEN {self.when2}"
                f"{' AND ' + self.and1 if self.and1 else ''}",
                f"THEN {self.then2}"
            ]
        )

    def write_docx(self, doc: Document):
        p = doc.add_paragraph()
        p.add_run("Scenario: ").bold = True
        r = p.add_run()
        r.add_break()

        p.add_run("GIVEN ").bold = True
        r = p.add_run(f"{self.given}")
        r.add_break()

        p.add_run("WHEN ").bold = True
        r = p.add_run(f"{self.when1}")
        r.add_break()

        p.add_run("THEN ").bold = True
        r = p.add_run(f"{self.then1}")
        r.add_break()

        p.add_run("WHEN ").bold = True
        r = p.add_run(f"{self.when2}")
        if self.and1:
            p.add_run(" AND ").bold = True
            r = p.add_run(f"{self.and1}")
        r.add_break()

        p.add_run("THEN ").bold = True
        r = p.add_run(f"{self.then2}")
        r.add_break()

