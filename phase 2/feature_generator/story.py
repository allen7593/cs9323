from docx import Document

from feature import Feature


class Story:
    name: str
    num: int
    features: list

    def __init__(self, name, story_num: int):
        self.name = name
        self.num = story_num
        self.features = list()

    def __call__(self, feature: Feature):
        self.features.append(feature)

    def __str__(self):
        return f"{self.num}. Story: {self.name}\n" + "\n\n".join([str(feature) for feature in self.features]) + "\n\n\n\n"

    def write_docx(self, doc: Document):
        doc.add_heading(f"{self.num}. Story: {self.name}", level=1)
        for feature in self.features:
            feature.write_docx(doc)
        pass