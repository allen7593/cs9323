# Phase 2 has started and will end at week 5(Detail date will be updated after asked the mentors)

## Final Handout
https://drive.google.com/file/d/1rEVONRdawHm-Glc5AaButFUp5K7XXfKC/view?usp=sharing

## Metors' Feedback:

(16.10.2019 updated)

* Reserve core features and delele or reorder lack-of-importance features;
* Add more details in features' description, especially specific instances, even fake examples
* Add interactions between components in System architectural diagram.
* Change "data" table's name in Database Structure.

## Phase 2 deliverables

### Features: 
- Identify Requirements

https://docs.google.com/spreadsheets/d/1jvM8Xhq3tgKMI5bc86MQVgp8miOgonjt2eSyWUiRiVw/edit?usp=sharing

### Design: 
- Enity-Relationship(ER) modle of data layer
- System architectural diagram

https://docs.google.com/document/d/1GKXOMPZA0RwDmQ9H41ATkgdKc3bt8V08EFwVfYQWm-I/edit?usp=sharing

![](Architecture.png)

### System Architectural Diagram(see above link↑)
https://drive.google.com/file/d/1FJilbj-AHBoAvEdGd9voo2Ke5NLjnY5V/view?usp=sharing


### Database Design
https://drive.google.com/file/d/1RPt8fc88078agO-DSa_rmC5oFj4dZwWT/view?usp=sharing  

** To change data table, we rename it to "Activity" and multiple different actiivyties (e.g. Notification, Team, projects) are inheritence from activity **

![picture](9323_database.jpg)

![picture](database_design.png)