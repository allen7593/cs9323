# Phase 3
## Deliverable:
- Working product
- Mockup for additional content

## PPT:
https://docs.google.com/presentation/d/1aPGHgDoVUvM5cSM9jflJ0ZMcGM6xU6pN/edit#slide=id.p5

## Presentation Points:
https://docs.google.com/document/d/1m-AMjTB7jiSx11NEGiNUuP5fyFq3yXTgyKBDqbKzuTY/edit?usp=sharing

## 23/10/2019 Meeting Notes:
https://drive.google.com/file/d/1yMXWcigz13MGxteU3iO1suVDvVpHMhGx/view?usp=sharing