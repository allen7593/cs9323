# Phase 4 improvements:
- Navigation font size
- Insert Demo data
- Notification Title
- Project Topic List
- Send Notification to students
- Add to future work
- JWT add role and permission

## Permission mappings:
https://docs.google.com/spreadsheets/d/1OPHXo3xQyyLff50vOxw1M28HvrQxQQMFrHfFruSAS4g/edit?usp=sharing